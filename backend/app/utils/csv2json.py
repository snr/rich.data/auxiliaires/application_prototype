import pandas as pd
import os
import re

from .constantes import DATA
from .functions import read_csv, write_json


# **************************************************************
# transform the csv file created in the 'data preparation'
# step into a json to use as the database for the application
# **************************************************************
    
    
def to_json(df: pd.core.frame.DataFrame, column_headers: list) -> dict:
    """
    transform the dataframe to a json file that will be
    used as the dataframe of the prototype
    
    output structure:
    -----------------
    {
        "row_index1": {
            "column_header1": "value1",
            "column_header2": "value2",
            ...
        },
        "row_index2": {
            ...
        }
    }
    
    :param df: the dataframe
    :param column_headers: the headers of the dataframe
    :returns: the df expressed as a dict
    """
    out = {}
    
    # NaN values don't work in JSON => replace them with `""`
    df = df.fillna("")
    
    # build the dictionnary
    for idx, row in df.iterrows():
        out[idx] = { k: v for k, v in zip(column_headers, row.to_list()) }

    # the last item is a string representation of a list of file paths => 
    # transform it into an actual list
    for k, v in out.items():
        v["path"] = re.sub("(^\[|\]$|')", "", v["path"]).split(",")
        out[k] = v
    return out


def csv2json_pipeline():
    """
    main pipeline function: read the csv as a dataframe,
    transform it into a json and save it
    """
    icono_headers = [
        # when using custom headers, the index column isn't named
        "url_manifest", "url_image", "type", "museum", "type_duplicate",  "author_1", 
        "author_2", "technique", "title", "date", "inventory_number", "url_manifest_duplicate", 
        "url_image_duplicate", "iconography", "title_secondary", "description", "inscriptions", 
        "part_of", "remarks", "historical_comment", "path"
    ]  # column headers for pandas
    df = read_csv(os.path.join(DATA, "iconographie.csv"), icono_headers)
    db = to_json(df, icono_headers)
    write_json(db, os.path.join(DATA, "db_iconographie.json"))
    return

