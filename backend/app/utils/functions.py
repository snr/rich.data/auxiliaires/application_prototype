import pandas as pd
import json
import os
import re

from .constantes import DATA


# *************************************************************
# globally available useful functions
# *************************************************************


def read_db() -> dict:
    """
    read and load the json database
    
    :returns: the database
    """
    return read_json(os.path.join(DATA, "db_iconographie.json"))


def read_json(fpath: str) -> dict:
    """
    read a json file as a dict
    
    :returns: the json file loaded as a dict
    """
    with open(fpath, mode="r") as fh:
        db = json.load(fh)
    return db


def write_json(db: dict, fpath: str) -> None:
    """
    write a dict to a json file
    
    :param db: the dictionnary database to write to a file
    :returns: none
    """
    with open(fpath, mode="w") as fh:
        json.dump(db, fh, indent=2)
    return


def read_csv(infile: str, column_headers: list) -> pd.core.frame.DataFrame:
    """
    read a csv file as a dataframe.
    the first column (renamed `index_column`) is an index, 
    the rest are series. 
    :param infile: the path to the input file
    :param column_headers: custom names for our headers
    :return: df, an initialized dataframe
    """
    return pd.read_csv(
        infile,
        sep="\t", quotechar="'", header=0, skip_blank_lines=True,
        names=column_headers, dtype="string", index_col=0
    )


def simplify_string(_in: str):
        """
        simplify a string: remove trailing/leading spaces, uper/lowercase,
        accented characters and double whitespace
        
        :param _in: the input string
        :return: the simplified input string
        """
        punct = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '-',
                 '+', '=', '{', '}', '[', ']', ':', ';', '"', "'", '|',
                 '<', '>', ',', '.', '?', '/', '~', '`']
        accents = {"é": "e", "è": "e", "ç": "c", "à": "a", "ê": "e",
                   "â": "a", "ô": "o", "ò": "o", "ï": "i", "ì": "i",
                   "ö": "o"}
        _in = _in.lower()
        for p in punct:
            _in = _in.replace(p, "")
        for k, v in accents.items():
            _in = _in.replace(k, v)
        _in = re.sub(r"\s+", " ", _in)
        _in = re.sub(r"(^\s|\s$)", "", _in)
        return _in  # true if same, false if not


def simplify_compare(string1: str, string2: str):
        """
        compare two strings to check if they're the same without punctuation,
        accented characters and capitals

        :param string1, string2: the strings to compare
        :return: true if there's a match, false if not
        """
        return (simplify_string(string1) == simplify_string(string1))  # true if same, false if not

