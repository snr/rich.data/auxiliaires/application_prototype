import os


# ***********************************************
# all constants are defined here
# ***********************************************


UTILS = os.path.abspath(os.path.dirname(__file__))
APP = os.path.abspath(os.path.join(UTILS, os.pardir))
DATA = os.path.abspath(os.path.join(APP, "data"))
ROUTES = os.path.abspath(os.path.join(APP, "routes"))
BACKEND = os.path.abspath(os.path.join(APP, os.pardir))
STATICS = os.path.abspath(os.path.join(APP, "statics"))
