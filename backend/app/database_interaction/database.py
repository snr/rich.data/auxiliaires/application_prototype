from pathlib import Path

from .validators import validate_db
from ..utils.functions import read_db, simplify_string


# ********************************************************
# main class to configure and search the database
# ********************************************************


class DataBase():
    """
    main database class: fetches and sends data through
    the middleware API to the client
    
    :attributes:
    - `db`: the whole database object, a dict
    
    :methods:
    - `__init__`: define the `db` attribute
    - `item_main`: get a single item by its id
    - `index`: build a lighter version of the db for the index page
    """
    def __init__(self):
        """
        validate and load the database
        """
        self.db = validate_db(read_db())
        return
    
    def item_index(self) -> dict[str, dict]:
        """
        return a lighter version of the database to
        build the index page
        
        return structure:
        -----------------
        {
            "item_id": {
                "author_1": "...",         # the first author
                "author_2": "...",         # the second author
                "title": "...",            # the title of the work
                "title_secondary": "...",  # a secondary title if it exists
                "date": "...",             # the work's creation date
                "path": "...",             # the path to the image
                "iiif_manifest": boolean   # wether there's a iiif manifest for the image
            },
            "item_id": {
                ...
            }
        }
        
        :returns: a lighter version of the database
        """
        out = {}
        for k, v in self.db.items():
            out[k] = {
                "author_1": v["author_1"],
                "author_2": v["author_2"],
                "title": v["title"],
                "title_secondary": v["title_secondary"],
                "date": v["date"],
                "path": v["path"][0],
                "iiif_manifest": True if v["url_manifest"] != "" else False
            }
        return out
        
    def item_main(self, id_: str) -> dict[str, dict]:
        """
        filter the database to only return the key-value pair
        with `id_` as key, in order to build the main page on
        a single item
        
        output structure:
        -----------------
        "id_": {
          author_1: "...",
          "author_2": "",
          date: "...",
          description: "...",
          "historical_comment": "",
          iconography: "...",
          inscriptions: "..."
          inventory_number: "...",
          museum: "...",
          "part_of": "...",
          "path": [
            "..."
          ],
          "remarks": "...",
          technique: "...",
          title: "...",
          title_secondary: "...",
          type: "...",
          type_duplicate: "...",
          url_image: "...",
          url_image_duplicate: "...",
          url_manifest: "...",
          url_manifest_duplicate: "..."
        }
        
        :param id_: the index to filter the database with
        :returns: a dict with info on a single item. see above
        """
        id_simp = id_.strip().lower()  # simplified identifier
        out = {}
        for k, v in self.db.items():
            if k.strip().lower() == id_simp:
                out[id_] = v
        return out
    
    def author_index(self) -> dict[str, str]:
        """
        build an index of authors: an author's name
        mapped to the filename of the first work by that author
        
        output structure
        ----------------
        {
            "author_name": "file_name",
            "author_name": "file_name"
        }
        """
        out = {}
        for v in self.db.values():
            # first, add `author_1`; then, add `author_2`; 
            # for each, add the first filename to display an image in the front end
            if v["author_1"] != "" and v["author_1"] not in out.keys():
                out[v["author_1"]] = ""
            if v["author_1"] != "" and v["path"][0] != "" and out[v["author_1"]] == "":
                out[v["author_1"]] = v["path"][0]

            if v["author_2"] != "" and v["author_2"] not in out.keys():
                out[v["author_2"]] = ""
            if v["author_2"] != "" and v["path"][0] != "" and out[v["author_2"]] == "":
                out[v["author_2"]] = v["path"][0]
            if v["author_2"] != "" and v["path"][0] != "" and out[v["author_2"]] == "":
                out[v["author_2"]] = v["path"][0]
        return out;
    
    def author_main(self, name) -> dict[str, dict]:
        """
        filter the database to only return all items by an author.
        
        output structure: a dict mapping an author's name to an `item_index` object
        ----------------
        {
            "author_name": {
                "item_id": {
                    "author_1": "...",         # the first author
                    "author_2": "...",         # the second author
                    "title": "...",            # the title of the work
                    "title_secondary": "...",  # a secondary title if it exists
                    "date": "...",             # the work's creation date
                    "path": "...",             # the path to the image
                    "iiif_manifest": boolean   # wether there's a iiif manifest for the image
                }
            }
        }
        """
        out = {name: {}}
        simp_name = simplify_string(name)
        item_db = self.item_index()  # shorter database, easier to process
        for k, v in item_db.items():
            # simplify and compare author names
            if (simp_name == simplify_string(v["author_1"])) == True \
            or (simp_name == simplify_string(v["author_2"])) == True:
                out[name][k] = v
        return out
