import traceback
import sys
import re


# *************************************************************
# functions to validate the outgoing data
# *************************************************************


def validate_item(item_index, item_data: {}):
    """
    check the validity of an entry in the database
    and clean this database entry if necessary
    
    :param item_index: the key of the item
    :param item_data: the data for an item
    """
    allowed_keys = [
        'url_manifest', 'url_image', 'type', 'museum', 'type_duplicate', 'author_1', 
        'author_2', 'technique', 'title', 'date', 'inventory_number', 
        'url_manifest_duplicate', 'url_image_duplicate', 'iconography',
        'title_secondary', 'description', 'inscriptions', 'part_of', 
        'remarks', 'historical_comment', 'path'
    ]
    # check that all keys are present
    if list(item_data.keys()).sort() != allowed_keys.sort():
        raise DataStructureException("data_keys", item_index, item_data.keys())
    
    # delete non-urls
    for k, v in item_data.items():
        if re.search("^url_.*", k) \
                and not re.search("^\s*$", v)\
                and not re.search("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", v):
            item_data[k] = ""
            # raise DataStructureException("url", item_index, {k: v})
    return item_data
    
def validate_db(data: {}):
    """
    validate the database object
    """
    for k, v in data.items():
        data[k] = validate_item(k, v)
    return data

class DataStructureException(TypeError):
    """
    this exception is raised if an object doesn't fit one of
    our data structures
    """
    def __init__(self, type: ["url", "data_keys"], idx: str, error_data):
        """
        print an error message
        
        :param type: the type of error
                     - url: the url isn't valid (currently, this option isn't active)
                     - data_keys: there are unallowed data keys in the object
        :param idx: the index of the database entry on which this error happened
        :param error_data: useful data to print to make the error make sense
        """
        print(f"""
            DATA STRUCTURE EXCEPTION:
            
            error of type: `{type}` on item `{idx}`
        """)
        if type == "url":
            print(f"""
                invalid url:
                
                `{error_data}`
            """) 
        elif type == "data_keys":
            print(f"""
            invalid keys:
            
            `{error_data}`
        """)
        sys.exit(1)
    
    