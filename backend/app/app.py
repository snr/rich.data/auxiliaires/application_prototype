from flask_cors import CORS
from flask import Flask
import os

from .database_interaction.database import DataBase
from .utils.csv2json import csv2json_pipeline
from .utils.constantes import STATICS, DATA


# ******************************************************
# configure the app
# ******************************************************


# create the database
if not os.path.isfile(os.path.join(DATA, "db_iconographie.json")):
    csv2json_pipeline()

# configure the app
app = Flask(
    "RICHDATA",
    static_folder=STATICS
)
app.config["SECRET_KEY"] = "this is a momentary secret key"
CORS(app)  # allow cross-origin requests to be sent to angular

# load and validate the database
database = DataBase()


from app.routes.routes_middleware import *

