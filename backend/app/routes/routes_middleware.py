from flask import url_for
import os

from ..app import app, database
from ..utils.constantes import STATICS


# ******************************************************
# generic routes
# all routes are prefixed with `mw`, short for
# `middleware` (since all of those routes are just
# api endpoints for angular to read it)
#
# static files (must be located in the folder declared
# as `statics_folder` in the app's configuration) can 
# be accessed by the frontend using the url 
# `${backend_root_url}/statics/${path_to_file}`
# ******************************************************


@app.route("/mw/item-index")
def route_item_index():
    """
    return the whole database to make a homepage
    """
    return database.item_index()


@app.route("/mw/item-main/<id_>")
def route_item_main(id_: str):
    """
    return the data on a unique item to build the
    main page for an item on angular
    
    :param id_: the identifier for that item
    """
    return database.item_main(id_)


@app.route("/mw/author-index")
def route_author_index():
    """
    return an index of image authors
    """
    return database.author_index()


@app.route("/mw/author-main/<name>")
def route_author(name: str) -> {}:
    """
    return data on an author
    
    :param name: the name of the author on which to return data
    """
    return database.author_main(name)


