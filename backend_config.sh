#!/usr/bin/env bash

# configure the backend : create a python env and install dependencies

root=$(pwd);
cd "$root/backend" && python3 -m venv env && source env/bin/activate && pip install -r "$root/backend/requirements.txt"

CAT << EOF 
python environment 'env' created and requirements installed.
launch the backend app with
  " bash launch_backend.sh "
and then laucn the frontend with
  " bash launch_frontend.sh "
EOF

