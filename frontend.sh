#!/usr/bin/env bash

# this script launches the frontend

root=$(pwd);

cd $root/frontend/ && ng serve
