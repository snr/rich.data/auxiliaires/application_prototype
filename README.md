# prototype de l'application du projet `RICHELIEU. HISTOIRE DU QUARTIER`

Ce prototype d'application a été développé avec Python-Flask et Angular, avant le 
développement de [l'application de présentation du projet](https://quartier-richelieu.inha.fr)
et l'application définitive, développée avec Python (Flask, SQLAlchemy) et Vue.js, 
à laquelle ce prototype sert de preuve de concept.

***VERSION 0.2.0 - 18.01.2022***

L'application Web du projet Richelieu, **Rich.Data**, vise à représenter l'iconographie 
produite sur le quartier Richelieu, à Paris (entre la Bourse et le Palais Royal). Inspirée 
par le *spatial turn*, cette application cherche à spatialiser les ressources iconographiques
pour faire l'étude historique et sociale du quartier, de la fin du XVIIIe siècle à
la fin du XIXe siècle.

D'un point de vue technique, l'application (dans sa version définitive) utilise une base de données 
`SQL`, un *backend* Flask interagissant avec la base de données, et un *frontend* `Angular` pour 
présenter les données. La communication entre *backend* et *frontend* se fait à l'aide d'une API 
REST.

**Pour un descriptif détaillé, voir plus bas**

![fonctionnement interne de l'application](./img/internals_application_20221123.png)
![chaîne actuelle](./img/pipeline_current_20230118.png)

---

## STRUCTURE DU DÉPÔT

```
racine/
  |__backend/ : le backend de l'application, en Python Flask
  |__frontend/ : le frontend de l'application, en Javascript/Typescript Angular
  |__presentation/ : les présentations et ressources autour de l'application
  |__*.sh: tous les scripts shell d'installation / désinstallation des dépendances
           nécessaires et les scripts pour lancer l'application
```

---

## INSTALLATION

```bash
git clone https://gitlab.inha.fr/snr/rich.data/application-prototype.git  # cloner le dépôt
cd application_prototype  # se déplacer dans le dépôt
sudo bash installs.sh  # installer nodejs et npm, si on a pas déjà ces logiciels (prend du temps)
bash backend_config.sh  # configurer l'application flask et installer les dépendances
```

---

## LANCER L'APPLICATION

Pour lancer l'application, plusieurs méthodes sont possibles:
- une méthode totalement automatique, qui demande de terminer manuellement 
  les processus `python` et `ng/npm` lancés en arrière plan
- une méthode semi-automatique, avec deux scripts `shell` lancés dans deux terminaux
- un lancement manuel. 

### Méthode semi-automatique (recommandée)

ouvrir deux terminaux et se déplacer dans le dossier `application_prototype`.

- **dans le premier terminal**, lancer `Flask` en entrant la commande:
  ```bash
  bash backend.sh
  ```
- **dans le second terminal**, lancer `Angular` en entrant la commande:
  ```bash
  bash frontend.sh
  ```

### Méthode automatique

Ouvrir un terminal dans la racine du dossier `application_prototype/` et entrer la commande:

```bash
bash ./run_background.sh
```

cette méthode a deux faiblesses: 
- le `stdout` et le `stderr` sont redirigés vers le fichier par défaut utilisé en sortie 
  de commande `nohup` (respectivement, `frontend/nohup.out` et `backend/nohup.out`). on ne 
  voit donc pas de potentiels messages d'erreur dans le terminal.
- il faut arrêter les applications *backend* et *frontend* manuellement. pour lister les
  processus à arrêter, entrer la commande ci-dessous (qui peut aussi faire ressortir 
  d'autres processus...)
  ```bash
  ps | grep -E "(python|serve)"
  ```

### Méthode manuelle

Avoir deux terminaux ouverts dans le dossier `application_prototype`.

- dans le **premier terminal**, lancer le *backend* en entrant les commandes:
  ```bash
  cd backend/  # se déplacer dans le dossier flask
  source env/bin/activate  # sourcer l'environnement python
  python run.py  # lancer l'application
  ```

- dans le **second terminal**, lancer le *frontend* en entrant les commandes:
  ```bash
  cd frontend/  # se déplacer dans le dossier angular
  ng serve -o  # lancer l'application
  ```

---

## FONCTIONNALITÉS (EN COURS ET À VENIR)

### Actuelles
- **chaîne de traitement (préparation des données avant l'application)**:
  - nettoyage automatique des données et des noms de fichiers
  - traduction des données du `csv` au `json` pour constituer une 
    base de données
  - enrichissements automatiques via des APIs pour constituer des jeux 
    de données
- **backend** (développé en `Python Flask`):
  - gestion d'une base de données en `json`
  - création d'une API pour envoyer des données et images au *frontend*
- **frontend** (développé en `Angular (TypeScript)`):
  - design d'interface
  - page d'accueil très scénarisée
  - navigation dans les catalogues des oeuvres et des artistes
  - mini-moteur de recherche: filtrage par mots clés, par caractéristiques
    et avec du texte libre
  - visionneuse IIIF (librairie javascript `OpenSeaDragon`)
  - cartographie Web (librairie javascript `Leaflet`) intégrant des fonds
    de cartes anciens de l'IGN (avec les protocoles `WMS` et `WMTS`) et 
    des images géoréférencées

### À venir
- **chaîne de traitement**:
  - créer une base de données `PostGreSQL` pour stocker les informations plus 
    durablement
  - créer des routines de gestion / production des données:
    - gérer la création de manifestes IIIF lorsque les institutions ne mettent pas
      de manifeste à disposition
    - gérer le traitement des images géoréférencées: à partir de `GeoTIFF`, produire
      des `png` et des informations sur le positionnement de ces images (à l'aide de
      `GeoJSON`, par exemple)
  - intégrer les informations issues d'institutions autres que Paris Musées
- **backend**:
  - redévelopper le *backend* pour qu'il puisse gérer la base de données PostGreSQL
    et gérer les différentes sources de données (base SQL, manifeste IIIF...)
  - étoffer les réponses HTTP de l'API: toujours définir des headers, des statuts de 
    réponse.
  - développer de nouvelles routes en fonction des besoins
- **frontend**:
  - étoffer la carte principale:
    - faire fonctionner la carte avec *toutes* les données et parcelles gérées par
      le site
    - créer des popups qui affichent toutes les images et informations liées à une 
      parcelle et permettre d'y naviguer
    - intégrer un système de filtrage chronologique des informations
  - créer des cartes secondaires: cartes spatialisant chaque oeuvre sur la page 
    principale d'une oeuvre, par exemple
  - créer des pages sur chaque parcelle: page superposant / montrant l'évolution
    de la parcelle à partir de différents relevés, par exemple
  - rajouter des textes, focus et contenus éditorialisés
  - mettre l'application en ligne sur `digital.inha`
  - gérer la gestion des erreurs

### À plus long terme
- intégrer des informations issues des bottins
- permettre aux utilisateur.ice.s de faire des sélections d'oeuvres, voire des exports
- développer une API, surtout pour les informations issues des bottins

---

## SCHÉMAS EXPLICATIFS

### Chaîne de traitement

La chaîne de traitement actuelle est la suivante:
![chaîne actuelle](./img/pipeline_current_20230118.png)

La chaîne de traitement prévisionnelle de la version finale de l'application correspond à ceci:
![chaîne finale](./img/pipeline_project_20230118.png).

### Modèle relationnel (prévisionnel)

![modèle relationnel](./img/richdata_modele_relationnel_20230203.png)

---

## REMARQUES GÉNÉRALES

- **noms de fichiers dans le dossier `img/`**:
  - pour la chaîne de traitement prévisionnelle: `pipeline_project_YYYYMMDD.(png|drawio)`
  - pour la chaîne de traitement actuelle: `pipeline_current_YYYYMMDD.(png|drawio)`
  - en général, essayer de toujours dater ses schémas pour faciliter le versionnage
- **backend**: pour des raisons de place, les images ne sont pas stockées dans ce dépot, mais
  sur le *cloud* INHA. pour utiliser l'application, mettre les images (avec les espaces dans
  les noms de fichiers remplacés par des `_`) dans le dossier `backend/app/statics/images/`.

- **frontend**: 
  - *à propos des redirections et des URI*: les URLs de certaines pages (notamment la page 
    `auteur/`) prennent du texte libre (des noms d'auteur.ice.s) en paramètres. ça risque
    de créer des urls [*unsafe*](https://stackoverflow.com/a/497972) contenant des caractères
    interdits. quand on redirige vers ce genre de pages (vers la page principale sur un.e 
    auteur.ice), il faut *sanitize* l'URL avec `encodeURIComponent()` (et créer une fonction
    dans le `*-component.ts` qui permette de nettoyer l'url directement depuis le template.
    lorsqu'on arrive sur la page vers laquelle on a été redirigé, il faut, dans le 
    `component.ts`, décoder l'URI avec `decodeURIComponent()` pour retrouver la forme 
    originelle de la chaîne de caractère.
    - pages concernées, vers lequelles des urls doivent être nettoyés : 
      - `author-main` (correspondant à l'URL `auteur/`)
  - *à propos de la récupération des images depuis le backend*:
    - l'URL vers l'image dans tous les `*thumbnail.component` est définie au niveau
      du *thumbnail*.
    - l'URL vers l'image principale pour une page (les images de `item/:id`) sont définies
      au niveau de ce composant (`item-main.component`) et sont stockées comme variables
      de la classe qui représente ce composant

---

## Versionnage et archivage dans anciennes versions

le développement de cette application s'appuie sur un cycle prototypage / 
présentation du prototype. plusieurs prototypes successifs sont donc 
réalisés. à la finalisation d'un nouveau prototype, faire une **`release`**
avec un numéro de version. pour archiver les anciennes versions, 3 types de 
branches existent:

- **branche `main`** : branche utilisée pour présenter un prototype 
  fonctionnel après sa présentation à l'équipe projet et avant la 
  finalisation d'un nouveau prototype
- **branche `dev`** : branche utilisée pour le protoype en cours de
  développement.
- **branche `vX.X.X_DDDDMMJJ`** : branches servant à archiver les 
  anciennes versions et anciens prototypes des applications, avec
  `X.X.X` le numéro de la `release` et `DDDDMMJJ` la date d'archivage
  de l'application au format DDDDMMJJ. (par exemple: `v0.1.0_20221123`,
  pour la première version du prototype, publié le 23 novembre 2022.

---

## Tutoriels et ressources utiles

### Fonds de carte, leaflet etc.

- [fonds de carte de l'IGN](https://www.geoportail.gouv.fr/thematiques/_fonds-de-carte#!)
- [autres fonds de carte de l'IGN](https://remonterletemps.ign.fr/)
- [plan cadastral contemporain de l'IGN](https://geoservices.ign.fr/parcellaire-express-pci)

### Connecter un *backend* `Flask` à un *frontend* `Angular`

- une [bonne introduction](https://balramchavan.medium.com/angular-python-flask-full-stack-demo-27192b8de1a3)
- un tutoriel un [peu plus complet](https://auth0.com/blog/using-python-flask-and-angular-to-build-modern-apps-part-1/)
- tutoriel [très complet](https://developer.okta.com/blog/2019/03/25/build-crud-app-with-python-flask-angular)
  d'une entreprise qui vend quand même beaucoup son produit.

### `node.js` et `Angular`

- **`node.js`**
  - [manuel de téléchargement](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions)
  - [liens de téléchargement](https://github.com/nodesource/distributions/blob/master/README.md)


