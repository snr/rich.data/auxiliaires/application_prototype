#!/usr/bin/env bash

# install curl
which curl &> /dev/null || sudo apt install curl

# install nodejs + angular
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash - &&\
apt-get install -y nodejs
npm install -g @angular/cli

if [ ! -d frontend ]; then
  mkdir frontend;
fi;
