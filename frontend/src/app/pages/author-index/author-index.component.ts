import { Component, OnInit } from '@angular/core';

import { BackendInteractionService } from "@app-services/backend-interaction.service";
import { AuthorIndex, UserFilter } from "@app-utils/interfaces";


/**
 * index for all authors of iconographic ressources
 * with a small search bar to filter the results based
 * on a user-inputted string 
 */


@Component({
  selector: 'app-author-index',
  templateUrl: './author-index.component.html',
  styleUrls: ['./author-index.component.css']
})
export class AuthorIndexComponent implements OnInit {
  db!: AuthorIndex;
  userFilter: UserFilter = new UserFilter();
  
  constructor( private backendInteractionService: BackendInteractionService ) { }
  
  ngOnInit(): void {
    /**
     * on init, fetch the database from the backend service
     */
    this.getData();
  }
  
  getData(): void {
    /**
     * fetch the database from the backend service
     */
    this.backendInteractionService.authorIndex().subscribe( res => {
      this.db = res;
    })
  }
  
  updateUserFilter(newUserFilter: UserFilter): void {
    /**
     * when the value of the text input in the 
     * child component `user-filter` changes, update
     * the value of `userFilter`
     * @param newUserFilter: the updated user filter
     */
    this.userFilter = newUserFilter;
  }


}
