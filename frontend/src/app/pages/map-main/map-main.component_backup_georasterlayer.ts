import { Component, OnInit, AfterViewInit } from '@angular/core';

// const GeoRaster = require("georaster");
// const GeoRasterLayer = require("georaster-layer-for-leaflet");
import * as GeoRaster from "georaster";
import * as GeoRasterLayer from "georaster-layer-for-leaflet";
import * as L from "leaflet";

import { urlToStatic } from "@app-utils/global-functions";


/**
 * main map of the application
 *
 * about leaflet:
 * - types: https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/leaflet/index.d.ts
 * - online tile layers: https://leafletjs.com/reference.html#tilelayer-url-template
 * - leaflet geotif plugins: 
 *   - https://github.com/stuartmatthews/leaflet-geotiff (no longer supported ?)
 *   - https://github.com/onaci/leaflet-geotiff-2 (couldn't make it work in several days)
 *   - https://www.npmjs.com/package/leaflet-canvaslayer-field
 *   - https://github.com/geotiff/georaster-layer-for-leaflet (used)
 * - using shapefiles with leaflet: https://gis.stackexchange.com/questions/264794/display-shapefile-with-leaflet-and-layer-control
 *
 *
 * leaflet geotiff and angular on stackoverflow:
 * - https://stackoverflow.com/questions/55610066/leaflet-geotiff-usage-in-angular-6-application
 * - https://stackoverflow.com/questions/58101607/unable-to-plot-geotiff-on-leaflet-geotiff
 * - https://stackoverflow.com/questions/41502713/how-to-add-images-on-map-using-leaflet-and-javascript
 *
 * **warning** : GeoRaster relies on packages with s e c u r i t y i s s u e s
 * (see `npm audit fix`)
 */


@Component({
  selector: 'app-map-main',
  templateUrl: './map-main.component.html',
  styleUrls: ['./map-main.component.css']
})
export class MapMainComponent implements OnInit, AfterViewInit {
  map!: L.Map;
  stamenTiles!: L.TileLayer;
  geoTiffLayers: any[] = [];
  geoTiffUrls: string[] = [
    /*"FRAN_0198_2883_L-medium_georef_recadr.tif",
    "FRAN_0198_3131_L-medium_georef_recadr.tif",
    "FRAN_0198_3127_L-medium_georef_recadr.tif",
    "FRAN_0198_2883_L-medium_georef.tif",
    "FRAN_0198_3131_L-medium_georef.tif",*/
    "FRAN_0198_3127_L-medium_georef.tif"
  ];
  
  ngOnInit(): void { }
  ngAfterViewInit(): void {
    this.buildGeoTifUrls();
    this.initMap();
  }
  
  initMap(): void {
    /**
     * initialize the map
     */
    // define the tiles
    this.stamenTiles = L.tileLayer(
      "https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png",
      { 
        minZoom: 3,
        maxZoom: 18,
        attribution: `attribution: 'Map tiles by <a href='http://stamen.com'>`
                     + `Stamen Design</a>, `
                     + `<a href='http://creativecommons.org/licenses/by/3.0'>`
                     + `CC BY 3.0</a> &mdash;`
      }
    )
    
    // define the map
    this.map = L.map("map-main", {
      center: [ 48.8687452, 2.3363674 ],
      zoom: 14.7
    })
    
    // add the layers
    this.stamenTiles.addTo(this.map);
    
    // add the raster image layers to the map
    this.geoTiffUrls.forEach( (url: string) => {
      fetch(url).then( res => res.arrayBuffer() )
                .then( arrayBuffer => {
                  
                  GeoRaster(arrayBuffer).then( (georaster: any) => {
                    console.log(georaster)
                    var layer = new GeoRasterLayer({
                      georaster: georaster,
                      opacity: 1,
                      // pixelValuesToColorFn: (values: []) => values[0] === 42 ? '#ffffff' : '#000000',
                      resolution: 64
                    });
                    
                    layer.addTo(this.map);
                    this.map.fitBounds( layer.getBounds() );
                  })
                })
    })
  }
  
  buildGeoTifUrls(): void {
    /**
     * build urls to the geotifs from filenames in
     * `this.geoTifUrls`
     */
    for ( let i=0; i < this.geoTiffUrls.length; i++ ) {
      this.geoTiffUrls[i] = urlToStatic(`images/geo/${this.geoTiffUrls[i]}`)
    }
  }
}
