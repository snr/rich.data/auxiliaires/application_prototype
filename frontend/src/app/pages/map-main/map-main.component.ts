import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as $ from "jquery";
import * as L from "leaflet";

import { MapService } from "@app-services/map.service";
import { urlToStatic } from "@app-utils/global-functions";


/**
 * main map of the application
 *
 * about leaflet:
 * - types: https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/leaflet/index.d.ts
 * - online tile layers: https://leafletjs.com/reference.html#tilelayer-url-template
 *
 * styling popups as sidebars:
 * - https://github.com/tomik23/leaflet-examples/tree/master/docs/59.sidebar-replacing-popup
 * - https://tomik23.github.io/leaflet-examples/#59.sidebar-replacing-popup
 *
 * about WMTS and WMS: 
 * - WMTS : the Web Map Tile Service is an API standard to use 
 *   and servce custom georeferenced tiles. it is used for custom map
 *   backgrounds and other georeferenced data
 *   - documentation: https://geoservices.ign.fr/documentation/services/api-et-services-ogc/images-tuilees-wmts-ogc
 * - WMS: same thing as WMTS, but it is slower: the tiles are calculated
 *   at each client request (with WMTS, tiles are precalculated)
 *   - IGN documentation: https://geoservices.ign.fr/documentation/services/api-et-services-ogc/images-wms-ogc
 *   - Leaflet documentation: https://leafletjs.com/examples/wms/wms.html
 *
 * about IGN (Institut Geograpique National), which allows to use map tiles
 * - leaflet + wmts (to add IGN map tiles): 
 *   https://geoservices.ign.fr/documentation/services/utilisation-web/affichage-wmts/leaflet-et-wmts
 * - list of IGN services, their access url and if a key is needed or not (to access WMS and WMTS services): 
 *   https://geoservices.ign.fr/documentation/services/tableau_ressources
 *   to get extra infos on a specific web service, use the `GetCapabilities` query indicated 
 *   in the dataset
 
 * leaflet geotif plugins: 
 * - https://github.com/stuartmatthews/leaflet-geotiff (no longer supported ?)
 * - https://github.com/onaci/leaflet-geotiff-2 (couldn't make it work in several days)
 * - https://github.com/geotiff/georaster-layer-for-leaflet (not working)
 * - https://www.npmjs.com/package/leaflet-canvaslayer-field
 * - using shapefiles with leaflet: https://gis.stackexchange.com/questions/264794/display-shapefile-with-leaflet-and-layer-control
 */


@Component({
  selector: 'app-map-main',
  templateUrl: './map-main.component.html',
  styleUrls: ['./map-main.component.css']
})
export class MapMainComponent implements OnInit, AfterViewInit {
  map!: L.Map;
  rasterUrls: string[] = [
    /*"FRAN_0198_2883_L-medium_georef_recadr.tif",
    "FRAN_0198_3131_L-medium_georef_recadr.tif",
    "FRAN_0198_3127_L-medium_georef_recadr.tif",
    "FRAN_0198_2883_L-medium_georef.tif",
    "FRAN_0198_3131_L-medium_georef.tif",
    "FRAN_0198_3127_L-medium_georef.tif"*/
    "FRAN_0198_2883_L-medium_georef.png"
  ];
  activeRasterOverlay?: L.ImageOverlay;
  popupDisplayed: boolean = false;
  popupWidth: number = 0;
  mapHeight: number = 0;
  mapWidth: number = 0;
  
  constructor ( private mapService: MapService) { }
  
  ngOnInit(): void { }
  ngAfterViewInit(): void {
    this.buildRasterUrls();
    this.initMap();
  }
  
  initMap(): void {
    /**
     * initialize the map
     * this is heavily adapted from the Javascript created by
     * a QGis to Leaflet export: https://plugins.qgis.org/plugins/qgis2web/
     * released under a GNU GPL 2.0 open license
     */
    // define the map
    this.map = L.map("map-main", {
      center: [ 48.8687452, 2.3363674 ]
      , maxBounds: L.latLngBounds([
        { lat: 48.89478506561126, lng: 2.2607803344726567 }
        , { lat: 48.8185413358923, lng: 2.4200820922851567 }
      ])
      , zoomControl: false
      , minZoom: 11  // 11 for paris + region, 13 for the neighbourhood
      , maxZoom: 17.7
      , zoom: 14.7
    });
    L.control.zoom({
      position: "topright"
    }).addTo(this.map);
    
    // define the panes (groups of layers defined by their `z-index`)
    this.map.createPane("basePane");  // default map tiles
    this.map.getPane("basePane")!.style.zIndex = "0";
    this.map.createPane("bgPane");  // custom background panes retrieved from WMS/WMTS
    this.map.getPane("bgPane")!.style.zIndex = "1";
    this.map.createPane("dataPane");  // actual data added on top of the map: raster images, shapes...
    this.map.getPane("dataPane")!.style.zIndex = "2";
    
    // define the base tiles
    const backgroundTiles = this.mapService.buildBackgroundTiles();
    for (let i=0; i < backgroundTiles.length; i++) {
      backgroundTiles[i].addTo(this.map)
    }
    // TODO : ADD GEOJSON TILE HERE TOO

    // build layer groups to enable / disable certain tiles and popups
    L.control.layers(
      {}, undefined, { hideSingleBase: true, position: "bottomright" }
    ).addOverlay(backgroundTiles[1], "Carte de l'etat-major 1&nbsp;: 40 000 (1820-1866)")
     .addOverlay(backgroundTiles[2], "Carte de Paris (1906)")
     .addOverlay(backgroundTiles[3], "Carte Alpage-Vasserot")       
     .addOverlay(backgroundTiles[4], "Atlas 1900")
     .addTo(this.map);
    
    // add the project-specific raster image layers to the map
    var rasterGroup: L.FeatureGroup = L.featureGroup([]);
    this.rasterUrls.forEach( (url: string) => {
      
      // set the bounds
      let bounds: L.LatLngBoundsExpression = [
        [48.86854291493092,2.339261942333602]
        ,[48.869103593490486,2.3402314265210684]
      ];
      
      // define the image overlay and set a popup 
      // that will show on `rasterOverlay`.
      let rasterOverlay: L.ImageOverlay = L.imageOverlay(
        url, bounds, { pane: "dataPane" }
      );
      
      // add the overlay to `rasterGroup` and `this.map`
      rasterGroup.addLayer(rasterOverlay);
      this.map.addLayer(rasterOverlay);
      this.mapService.setBounds(rasterGroup, this.map);  // optionnal: center on the raster images
      this.mapService
          .buildImageOverlayPopup(rasterOverlay)
          .on("click", (e) => {
            // on click, open or close the popup
            this.popupDisplayed = !this.popupDisplayed;
            [this.popupWidth, this.mapHeight, this.mapWidth] = this.mapService.getWidths();
            
            // depending on wether we're opening or
            // closing the popup, `this.activeRasterOverlay`
            // must be defined before or after `mapService.repositionMap()`
            // : this.activeRasterOverlay must always be defined 
            // in `mapService.repositionMap()`...
            if ( this.popupDisplayed === true ) {
              this.activeRasterOverlay = e.target;
              this.mapService.repositionMap(
                this.map, this.activeRasterOverlay,
                this.popupDisplayed, this.mapWidth
              );
            } else {
              this.mapService.repositionMap(
                this.map, this.activeRasterOverlay,
                this.popupDisplayed, this.mapWidth
              );
              this.activeRasterOverlay = undefined;  
            }
            
          })
          .addTo(this.map);
    });
    rasterGroup.addTo(this.map);
  }
  
  buildRasterUrls(): void {
    /**
     * build urls to the geotifs from filenames in
     * `this.rasterUrls`
     */
    for ( let i=0; i < this.rasterUrls.length; i++ ) {
      this.rasterUrls[i] = urlToStatic(`images/geo/${this.rasterUrls[i]}`)
    }
  }
  
  closePopupOnEvent(popupDisplayStatus: boolean): void {
    /**
     * catch the popup closing events and close the popup 
     * - pressing the `.xcross` button in `map-popup-main` 
     * - pressing the `escape` key in this component
     */
    this.popupDisplayed = popupDisplayStatus;
    this.mapService.repositionMap(
      this.map, this.activeRasterOverlay, 
      this.popupDisplayed, this.mapWidth
    )
    this.activeRasterOverlay = undefined;
  }
}
