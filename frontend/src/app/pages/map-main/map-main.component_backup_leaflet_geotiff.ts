import { Component, OnInit, AfterViewInit } from '@angular/core';

// import * as Geotiff from "geotiff";
// const Geotiff = require("geotiff");


// import * as LeafletGeotiff from "leaflet-geotiff-2";
// const LeafletGeotiff = require("leaflet-geotiff-2");  // there are issues with normal imports

// import * as Plotty from "plotty";
// const Plotty = require('leaflet-geotiff-2/dist/leaflet-geotiff-plotty');

import * as L from "leaflet";

import { urlToStatic } from "@app-utils/global-functions";


/**
 * main map of the application
 *
 * about leaflet:
 * - types: https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/leaflet/index.d.ts
 * - online tile layers: https://leafletjs.com/reference.html#tilelayer-url-template
 * - leaflet geotif plugins: 
 *   - https://github.com/stuartmatthews/leaflet-geotiff (no longer supported ?)
 *   - https://github.com/onaci/leaflet-geotiff-2 (used)
 *   - https://www.npmjs.com/package/leaflet-canvaslayer-field
 *   - https://github.com/geotiff/georaster-layer-for-leaflet
 * - using shapefiles with leaflet: https://gis.stackexchange.com/questions/264794/display-shapefile-with-leaflet-and-layer-control
 *
 * leaflet geotiff and angular on stackoverflow:
 * - https://stackoverflow.com/questions/55610066/leaflet-geotiff-usage-in-angular-6-application
 * - https://stackoverflow.com/questions/58101607/unable-to-plot-geotiff-on-leaflet-geotiff
 * - 
 *
 * **warning** : GeoRaster relies on packages with s e c u r i t y i s s u e s
 * (see `npm audit fix`)
 */


@Component({
  selector: 'app-map-main',
  templateUrl: './map-main.component.html',
  styleUrls: ['./map-main.component.css']
})
export class MapMainComponent implements OnInit, AfterViewInit {
  map!: L.Map;
  stamenTiles!: L.TileLayer;
  geoTiffLayers: any[] = [];
  geoTiffUrls: string[] = [
    "FRAN_0198_2883_L-medium_georef_recadr.tif",
    "FRAN_0198_3131_L-medium_georef_recadr.tif",
    "FRAN_0198_3127_L-medium_georef_recadr.tif",
    "FRAN_0198_2883_L-medium_georef.tif",
    "FRAN_0198_3131_L-medium_georef.tif",
    "FRAN_0198_3127_L-medium_georef.tif"
  ];
  
  ngOnInit(): void { }
  ngAfterViewInit(): void {
    this.buildGeoTifUrls();
    this.initMap();
  }
  
  initMap(): void {
    /**
     * initialize the map
     */
    // define the tiles
    this.stamenTiles = L.tileLayer(
      "https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png",
      { 
        minZoom: 3,
        maxZoom: 18,
        attribution: `attribution: 'Map tiles by <a href='http://stamen.com'>`
                     + `Stamen Design</a>, `
                     + `<a href='http://creativecommons.org/licenses/by/3.0'>`
                     + `CC BY 3.0</a> &mdash;`
      }
    )
    
    // define the geotif layers and options
    /* LEAFLET GEOTIFF 2 - MY ATTEMPT */
    // const renderer = (L as any).LeafletGeotiff.rgb();
    /*this.geoTiffUrls.forEach( (url: string) => {
      let options = {
        band: 0,
        name: url,
        opacity: 1,
        // image: url,
        onError: () => { console.log(`error loading ${url}`)},
        // bounds: [[40.712216, -74.22655], [40.773941, -74.12544]],
        // renderer: renderer
        // renderer: new plotty.Plotty({
        //   colorScale: 'greys'
        // })
      }
      this.geoTiffLayers.push( (L as any).leafletGeotiff(url, options) )
      new (L as any).leafletGeotiff.plotty(options);
    })*/
    
    /* LEAFLET GEOTIFF 2 - MWE */
    /* const url = "https://stuartmatthews.github.io/leaflet-geotiff/tif/wind_speed.tif";
    const options = {
      // See renderer sections below.
      // One of: L.LeafletGeotiff.rgb, L.LeafletGeotiff.plotty, L.LeafletGeotiff.vectorArrows
      renderer: null,
    
      // Use a worker thread for some initial compute (recommended for larger datasets)
      useWorker: false,
    
      // Optional array specifying the corners of the data, e.g. [[40.712216, -74.22655], [40.773941, -74.12544]].
      // If omitted the image bounds will be read from the geoTIFF file (ModelTiepoint).
      bounds: [],
    
      // Optional geoTIFF band to read
      band: 0,
    
      // Optional geoTIFF image to read
      image: 0,
    
      // Optional clipping polygon, provided as an array of [lat,lon] coordinates.
      // Note that this is the Leaflet [lat,lon] convention, not geoJSON [lon,lat].
      clip: undefined,
    
      // Optional leaflet pane to add the layer.
      pane: "overlayPane",
    
      // Optional callback to handle failed URL request or parsing of tif
      onError: null,
    
      // Optional, override default GeoTIFF function used to load source data
      // Oneof: fromUrl, fromBlob, fromArrayBuffer
      // sourceFunction: GeoTIFF.fromUrl,
    
      // Only required if sourceFunction is GeoTIFF.fromArrayBuffer
      arrayBuffer: null,
    
      // Optional nodata value (integer)
      // (to be ignored by getValueAtLatLng)
      noDataValue: undefined,
    
      // Optional key to extract nodata value from GeoTIFFImage
      // nested keys can be provided in dot notation, e.g. foo.bar.baz
      // (to be ignored by getValueAtLatLng)
      // this overrides noDataValue, the nodata value should be an integer
      noDataKey: undefined,
    
      // The block size to use for buffer
      blockSize: 65536,
    
      // Optional, override default opacity of 1 on the image added to the map
      opacity: 1,
    
      // Optional, hide imagery while map is moving (may prevent 'flickering' in some browsers)
      clearBeforeMove: false
    };
    */
    
    // define the map
    this.map = L.map("map-main", {
      center: [ 48.8687452, 2.3363674 ],
      zoom: 14.7
    })
    
    // add the layers
    this.stamenTiles.addTo(this.map);
    
    /* LEAFLET GEOTIFF 2 - MY ATTEMPT */
    this.geoTiffUrls.forEach( (url: string) => {
      let options = {
        band: 0,
        name: url,
        opacity: 1,
        // image: url,
        onError: () => { console.log(`error loading ${url}`)},
        // bounds: [[40.712216, -74.22655], [40.773941, -74.12544]],
        // renderer: renderer
        // renderer: new plotty.Plotty({
        //   colorScale: 'greys'
        // })
      };
      var layer = (L as any).leafletGeotiff(url, options).addTo(this.map)
      // var layer = LeafletGeotiff.leafletGeotiff(url, options).addTo(this.map)
      // new (L as any).leafletGeotiff.plotty(options);
    })
    /*this.geoTiffLayers.forEach( (layer) => {
      layer.addTo(this.map);
    })*/
    
    /* LEAFLET GEOTIFF 2 - MWE */
    /*this.map.on("load", () => {
      // (L as any) allows to bypass type definitions for leaflet
      new (L as any).leafletGeotiff(url, options)
                        .addTo(this.map);// L.leafletGeotiff(url, options).addTo(map);
    })*/
  }
  
  buildGeoTifUrls(): void {
    /**
     * build urls to the geotifs from filenames in
     * `this.geoTifUrls`
     */
    for ( let i=0; i < this.geoTiffUrls.length; i++ ) {
      this.geoTiffUrls[i] = urlToStatic(`images/geo/${this.geoTiffUrls[i]}`)
    }
  }
}
