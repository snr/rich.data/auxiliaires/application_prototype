import { Component, OnInit } from '@angular/core';

import { BackendInteractionService,  } from "@app-services/backend-interaction.service";
import { ItemIndex, UserFilter } from "@app-utils/interfaces";


/**
 * focus sur la bourse sur la présentation banque de France, à supprimer
 */
 

@Component({
  selector: 'app-bourse',
  templateUrl: './bourse.component.html',
  styleUrls: ['./bourse.component.css']
})
export class BourseComponent implements OnInit {
  db!: ItemIndex;
  userFilter: UserFilter  = {textFilter: "", locationFilter: "bourse", iiifOnly: false};

  constructor( private backendInteractionService: BackendInteractionService ) { }
  
  ngOnInit(): void {
    this.getData();
    
  }

  getData(): void {
    /**
     * load the database object from the backend
     */
    this.backendInteractionService.itemIndex().subscribe( res => {
      this.db = res;
    })
  }
  
  updateUserFilter(newUserFilter: UserFilter): void {
    /**
     * when the value of the text input in the 
     * child component `user-filter` changes, update
     * the value of `userFilter`
     *
     * append `bourse` each time to only show data on the Bourse
     * 
     * @param newUserFilter: the updated user filter
     */
    newUserFilter.locationFilter = "bourse";
    this.userFilter = newUserFilter;
  }
}

