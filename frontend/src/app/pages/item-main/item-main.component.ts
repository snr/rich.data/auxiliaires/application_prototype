import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ParamMap } from "@angular/router";

import { BackendInteractionService } from "@app-services/backend-interaction.service";
import { urlToStatic } from "@app-utils/global-functions";
import { Item } from "@app-utils/interfaces";


/**
 * main page for a single item in the database
 */


@Component({
  selector: 'app-item-main',
  templateUrl: './item-main.component.html',
  styleUrls: ['./item-main.component.css']
})
export class ItemMainComponent implements OnInit {
  id!: string;
  db!: Item;
  imageUrl: string[] = [];
  popup: boolean = false;
  popupImageUrl?: string;

  @Input() iiifLoaded: boolean = false;  // toggle the showing of the loader

  constructor( 
    private backendInteractionService: BackendInteractionService,
    private route: ActivatedRoute
  ) { }
  
  ngOnInit(): void {
    this.route.paramMap.subscribe( (params: ParamMap) => {
      this.id = params.get("id") as string;
      this.getData();
    })
  }
  
  getData(): void  {
    /**
     * fetch data from the backend and initialize 
     * all data variables:
     * - `db`, the complete data on that object
     * - `id`, the identifier for that object
     * - `imageUrl`, an array of urls pointing 
     *   to the images of the ressources.
     */ 
    this.backendInteractionService.itemMain(this.id).subscribe( res => {
      this.db = res as Item;
      const fnameArray: string[] = this.db[this.id].path;  // array of filenames of the image
      fnameArray.forEach(f => {
        if (f !== undefined && f !== "" ) {
          // only append non-empty items
          this.imageUrl.push(urlToStatic(`images/${f}`));
        }
      })
    })
  }
  
  imagePopup(imgUrl: string): void {
    /**
     * toggle the popup
     */
    this.popup = !(this.popup);
    if (this.popup === true ) {
      this.popupImageUrl = imgUrl;  
    }
  }
  
  toUrl(input: string): string {
    /**
     * encode a string as an url component
     * (used to redirect to author paes)  
     */
    return encodeURIComponent(input);
  }

}