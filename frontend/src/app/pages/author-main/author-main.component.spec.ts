import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorMainComponent } from './author-main.component';

describe('AuthorMainComponent', () => {
  let component: AuthorMainComponent;
  let fixture: ComponentFixture<AuthorMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthorMainComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AuthorMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
