import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from "@angular/router";

import { BackendInteractionService } from "@app-services/backend-interaction.service";
import { Author, UserFilter } from "@app-utils/interfaces";


/**
 * main page on an author: display a list of their works,
 * with a search bar to filter which works are displayed
 */



@Component({
  selector: 'app-author-main',
  templateUrl: './author-main.component.html',
  styleUrls: ['./author-main.component.css']
})
export class AuthorMainComponent implements OnInit {
  db!: Author;
  id!: string;
  userFilter: UserFilter  = new UserFilter();
  
  constructor(
    private route: ActivatedRoute,
    private backendInteractionService: BackendInteractionService
  ) { }
  
  ngOnInit(): void {
    this.route.paramMap.subscribe( (params: ParamMap) => {
      this.id = decodeURIComponent(params.get("id") as string);  // decode the url encoded name
      this.getData();
    })
  }
  
  getData(): void {
    /**
     * fetch data from the backend and initialize 
     * all data variables
     * - `db`, the database holding all items related to the author
     */
    this.backendInteractionService.authorMain(this.id).subscribe( res => {
      this.db = res;
    })
  }
  
  updateUserFilter(newUserFilter: UserFilter): void {
    /**
     * when the value of the text input in the 
     * child component `user-filter` changes, update
     * the value of `userFilter`
     * @param newUserFilter: the updated user filter
     */
    this.userFilter = newUserFilter;
  }
}
