import { Component, OnInit, HostListener, ChangeDetectionStrategy } from '@angular/core';
import { Observable, of } from "rxjs";
import isEqual from 'lodash.isequal';
import * as $ from "jquery";

import  { ScrollService } from "@app-services/scroll.service";
import { ComponentCoordinates } from "@app-utils/interfaces";
import { urlToStatic } from "@app-utils/global-functions"


/**
 * home page, with an intro banner and several views presenting the project
 */


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomepageComponent implements OnInit {
  homeImageUrl!: string;
  introImageUrl!: { [key: string]: string };
  introHoverImageUrl!: string;
  viewPositions: ComponentCoordinates[] = [];  // array of positions of the different views, for the scroll service
  currentView!: ComponentCoordinates // HomePageView;  // to calculate the pre-scroll position: must match the html element's id
  scrolling: boolean = false;  // boolean indicating if we're scrolling or not
  palaisDeCristal: string = urlToStatic("images/homepage_palais_de_cristal.jpeg")
  
  constructor( private scrollService: ScrollService ) { }
  
  ngOnInit(): void {
    // this.currentView = "intro";
    this.getIntroImages().subscribe( res => {
      this.homeImageUrl = res[0] as string;
      this.introImageUrl = res[2] as { [key: string]: string };
      this.introHoverImageUrl = res[1] as string;
    })
    this.currentView = this.getPositionById("intro");
    this.defineViewPositions();
    this.definePresentationBoxesPositions();
  }


  /****************************************************
   *                    STYLE RULES                   * 
   ****************************************************/
  getIntroImages(): Observable<Array<string | { [key: string]: string }>> {
    /**
     * build urls for the intro page and home page urls
     * @returns : an array with, at index 0, the home page url
     *            , at index 1, the intro page hover url
     *            , at index 2, a dict mapping ids to paths of intro images 
     */
    // let homeUrl = urlToStatic("images/_home_background_caricature.jpg");  /* original caricature */
    let homeUrl = urlToStatic("images/MC_22.jpg");  /* revolutionnary image */
    let introTurgotHoverUrl = urlToStatic("images/_intro_background_turgot.png");
    
    let introUrl: { [key: string]: string } = {};
    let n: number = 0;
    ["MC_108.png", "MC_129.jpg", "MC_158.jpg", "MC_125.jpg", "MC_124.jpg"]
    .forEach( (fname: string) => {
      introUrl[n] = urlToStatic(`images/${fname}`)
      n++
    })
    return of([homeUrl, introTurgotHoverUrl, introUrl])
  }
  
  definePresentationBoxesPositions(): void {
    /**
     * position all the `.presentation-box` elements
     * by aligning them alternatively to the left/right
     * of their parent
     *
     * this function is only activated on large screen
     * (media query: (`min-width: 800px`))
     */
    const mediaQuery =  window.matchMedia("screen and (min-width: 800px)");
    if ( mediaQuery.matches ) {
      let position: boolean = false;
      const boxes = $(".presentation-box");
      Object.keys(boxes).forEach( (k: any) => {
        if (! isNaN(k) ) {
          // isNaN only keeps the elements: other keys are 
          // present in the `boxes` object
          position === true ? boxes[k].style.left = "50%" 
                            : boxes[k].style.left = "0%";
          position = !position;
        }
      })
    }
  }
  
  /**
   * when hovering over one of the images of the banner,
   * toggle the opacity to show the below image
   */
  introMouseover(_id: string): void {
    _id = `#${_id}`;
    $(_id).css({ opacity: 0 });
  }
  introMouseout(_id: string): void {
    _id = `#${_id}`;
    $(_id).css({ opacity: 1, transition: "opacity 0.5s"});
  }
  

  /****************************************************
   *                     SCROLLING                    * 
   ****************************************************/
  scrollTo(el: Element) {
    /**
     * scroll to the element `el`. to be used in the template
     * @param el: the element to scroll to, named like this: `<div #elName>`
     */
    el.scrollIntoView({ behavior: "smooth" })
  }
    
  defineViewPositions(): void {
    /**
     * define the positions of the different 
     * (relative to the document, NOT TO THE VIEWER)
     * views to which we can scroll
     */
    this.viewPositions = this.scrollService.reorderViewPositions([
      this.getPositionById("intro"),
      this.getPositionById("homepage1"),
      this.getPositionById("homepage2"),
      this.getPositionById("homepage3")
    ])  // instantly reordered by value of `.top`
  }
  
  getPositionById(_id: string): ComponentCoordinates {
    /**
     * get the offset (X and Y coordinates, relative to
     * the document and NOT TO THE VIEWER) of an element 
     * based on its id
     */
    try {
      const position = $(`#${_id}`).offset() as { top: number, left: number };
      return { name: _id, top: position.top, left: position.left }
  
    } catch(e) {
      console.log(_id);
      const position = {top: 0, left: 0}
      return { name: _id, top: position.top, left: position.left }
    }
  }
  
  getViewerPositionById(_id: string): ComponentCoordinates {
    /**
     * define the position of an element, relative
     * to the client's viewer (aka, screen), based on
     * its id.
     */
    const el: DOMRect = document.getElementById(_id)!.getBoundingClientRect()
    return {name: _id, top: el.top, left: el.left}
  }
  
  scrollToTarget(postScrollPosition: number = 0): void {
    /**
     * define the positions of the different 
     * views to which we can scroll
     */
    const viewTarget: ComponentCoordinates = this.scrollService.findTarget(this.currentView, postScrollPosition, this.viewPositions)
    // const elId: string = viewTarget.name;  // the target element's id
        
    // every 0.5 seconds, check if the view shown is correct to
    // toggle the `this.scrolling` flag
    const i = setInterval(() => {
      var targetPosition: ComponentCoordinates = this.getViewerPositionById(viewTarget.name);
      if ( targetPosition.top === 0 ) {
        this.scrolling = false;
        clearInterval(i);
      };
    }, 500);
    i;
        
    // scroll to the target element
    if ( ! isEqual(this.currentView, viewTarget) ) {
      document.getElementById(viewTarget.name)!.scrollIntoView({ behavior: "smooth" })
      this.currentView = viewTarget;
    }
  }
  
  scrollToNext(currentViewName: string|undefined): void { 
    /**
     * get the current view and scroll to the next one
     * the current view can be targeted by its name using
     * the `currentViewName` parameter: this allows to explicitly
     * state the current view. when scrolling and using buttons
     * with `scrollToNext`, things will quickly break down if
     * `currentViewName` is not specified.
     * @param currentViewName: the name of the current view
     */
    // console.log("next", currentViewName, this.currentView.name)
    let currentView: ComponentCoordinates;
    if ( currentViewName !== undefined ) {
      // get the current view from `currentViewName`
      let o = this.scrollService.getCurrentViewByName(currentViewName, this.viewPositions)
      o === undefined ? currentView = this.currentView : currentView = o;
      const next: ComponentCoordinates|null = this.scrollService.findNext(currentView, this.viewPositions)
      if ( next !== null ) {
        // console.log("hello next")
        document.getElementById(next.name)!.scrollIntoView({ behavior: "smooth" })
      }
    }
  }
  
  scrollToPrev(currentViewName: string|undefined): void {
    /**
     * get the current view and scroll to the previous one
     * the `currentViewName` parameter: this allows to explicitly
     * state the current view. when scrolling and using buttons
     * with `scrollToNext`, things will quickly break down if
     * `currentViewName` is not specified.
     * @param currentViewName: the name of the current view
     */
    // console.log("prev", currentViewName, this.currentView.name)
    let currentView: ComponentCoordinates;
    if ( currentViewName !== undefined ) {
      // get the current view from `currentViewName`
      let o = this.scrollService.getCurrentViewByName(currentViewName, this.viewPositions)
      o === undefined ? currentView = this.currentView : currentView = o;
      const prev: ComponentCoordinates|null = this.scrollService.findPrev(currentView, this.viewPositions)
      if ( prev !== null ) {
        // console.log("hello prev")
        document.getElementById(prev.name)!.scrollIntoView({ behavior: "smooth" })
      }
    }
  }
  
  @HostListener("window:scroll", ["$event"]) onScroll(e: Event): void {
    /**
     * listen to scrolls in the page and scroll to another element
     */
    if ( this.scrolling === false ) {
      this.scrolling = true;
      this.scrollToTarget((e.target as Document).scrollingElement!.scrollTop)
    } 
  }
  
  @HostListener("window:resize", ["$event"]) onResize(e: Event): void {
    this.defineViewPositions();
  }
}
