import { Component, OnInit } from '@angular/core';

import { BackendInteractionService,  } from "@app-services/backend-interaction.service";
import { ItemIndex, UserFilter } from "@app-utils/interfaces";


/**
 * index: display information on all items,
 * with a small search bar
 * on this component is decided the filtering of 
 * results based on a user inputted string
 */
 

@Component({
  selector: 'app-item-index',
  templateUrl: './item-index.component.html',
  styleUrls: ['./item-index.component.css']
})
export class ItemIndexComponent implements OnInit {
  db!: ItemIndex;
  userFilter: UserFilter  = new UserFilter();  // the user's custom filters

  constructor( private backendInteractionService: BackendInteractionService ) { }
  
  ngOnInit(): void {
    this.getData();
    
  }

  getData(): void {
    /**
     * load the database object from the backend
     */
    this.backendInteractionService.itemIndex().subscribe( res => {
      this.db = res;
    })
  }
  
  updateUserFilter(newUserFilter: UserFilter): void {
    /**
     * when the value of the text input in the 
     * child component `user-filter` changes, update
     * the value of `userFilter`
     * @param newUserFilter: the updated user filter
     */
    // this.userFilterService.getUserFilter().subscribe( res => {
    // this._userFilter = res;
    // return this._userFilter;
    this.userFilter = newUserFilter;
  }
}