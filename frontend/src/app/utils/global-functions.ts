import { BACKEND_URL } from "@app-utils/constants";


/**
 * globally useful functions
 */


export function simplifyString(input: string): string {
  /**
   * simplify a string to perform string comparisons
   * remove punctuation, capitals, replace accented letters
   * with non-accented ones, strip spaces and double whitespace
   * @param input: the string to process
   * @returns: the simplified string
   */
  const punct: string[] = [
    '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '-',
    '+', '=', '{', '}', '[', ']', ':', ';', '"', "'", '|',
    '<', '>', ',', '.', '?', '/', '~', '`'
  ]
  const accents: { [key: string] : string } = {
    "é": "e", "è": "e", "ç": "c", "à": "a", "ê": "e",
    "â": "a", "ô": "o", "ò": "o", "ï": "i", "ì": "i",
    "ö": "o"
  }
  input = input.toLowerCase();
  const inputArr = Array.from(input);
  inputArr.forEach( (s: string) => {
    if (punct.includes(s)) {
      input = input.replaceAll(s, "");
    }
    if (s in accents) {
      input = input.replaceAll(s, accents[s]);
    }
  });
  return input.replaceAll(/\s+/g, " ")
              .replaceAll(/(^\s+|\s+$)/g, "")
}

export function matchStrings(input1: string, input2: string): boolean {
  /**
   * compare the two strings `string1` and `string2`
   * after having simplified them
   * @returns: `true` if the two strings match, `false`
   *           else/
   */
   return ( simplifyString(input1) === simplifyString(input2) );
}

export function urlToStatic(path: string): string {
  /**
   * build a url in a static file located in Flask's
   * `static_folder`.
   * @param path: the path to the file, from within the `
                  static_folder`
   * @returns: the url to the file
   */
  let out: string = ""
  if ( path !== undefined && path !== "" ) {
    out = `${BACKEND_URL}/statics/${path}`
  }
  return out;
}

export function instanceOfKeyValue(input:  {}): boolean {
  /**
   * check that an object fits to be used with
   * `kv2json()`: it has two keys, one labeled
   * `key` and the other `value`
   * @param input: the object to check
   */
  return ("key" in input && "value" in input && Object.keys(input).length === 2);
}

export function kv2json(input: {}): {} {
  /**
   * transform a key-value-piped object into what it was
   * originally.
   * @param input: the string representation of the json.
   * @returns: the object built out of the string
   * @example: {key: "prenom", value: "laura"} => {prenom: "laura"}
   */
  let out: { [key: string]: any } = {};
  try {
    if (
      input !== undefined 
      && instanceOfKeyValue(input) === true
    ) {
      let typedInput = input as {key: string, value: string};
      out[typedInput["key"]] = typedInput["value"];
    }
  } catch(e) {
    out = {}
  }
  return out;
}

export function getFirstKey( obj: {} ): string {
  /**
   * get the first key in an object
   */
  const keys: string[] = Object.keys(obj)
  if ( keys.length > 0 ) {
    return keys[0]
  } else {
    return ""
  }
}
