/** 
 * all interfaces and custom types used by the app
 */ 


export interface Item {
  [ key: string ]: {
    url_manifest: string,
    url_image: string,
    type: string,
    museum: string,
    type_duplicate: string,
    author_1: string,
    author_2: string,
    technique: string,
    title: string,
    date: string,
    inventory_number: string,
    url_manifest_duplicate: string,
    url_image_duplicate: string,
    iconography: string,
    title_secondary: string,
    description: string,
    inscriptions: string,
    part_of: string,
    remarks: string,
    historical_comment: string,
    path: string[]
  }
}

// database for the `item-index` page, containing all entries 
// with less information than `ItemDb` to make it lighter
// an item's id is mapped to data on that item.
export interface ItemIndex {
  [ key: string ]: {
    author_1: string,
    author_2: string,
    title: string,
    title_secondary: string,
    date: string,
    path: string,
    iiif_manifest: boolean
  }
}

// database for the `author-index` page
// an author's name is mapped to a filename
export interface AuthorIndex {
  [ key: string ]: string 
}


// database for the `author-main` page,
// containing the author's name and an index of their works
// an authors's name is mapped to an `ItemIndex` object
export interface Author {
  [ key: string ]: ItemIndex
}


// object that holds all user filters that define which results to display
// defined as a class to be able to assign default variables
export class UserFilter {
  textFilter: string = "";  // filter by a text string
  locationFilter: string = "";  // filter by location
  iiifOnly: boolean = false;
}

export type PageType = "authorMain"|"authorIndex"|"itemMain"|"itemIndex"

// the current view in the homepage. `intro` is the banner when
// we arrive on the website, `main` is the div with the image
// background, `presentation` is a small presentation of the
// website that comes after `main`
export type HomePageView = "intro"|"main"|"presentation"

// coordinates of a single component in a document.
// maps an html id to coordinates
export interface ComponentCoordinates {
  name: string,
  top: number, 
  left: number
}

// root urls of `wms` and `wmts` services
export interface webMapRootUrl {
  ignWms: string,
  ignWmts: string
}

// keys for the root urls of `wms` and wmts services
export type webMapRootUrlKey = "ignWms"|"ignWmts"
