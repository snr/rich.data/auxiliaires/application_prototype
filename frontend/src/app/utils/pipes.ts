import { Pipe, PipeTransform } from "@angular/core";
import isEqual from 'lodash.isequal';

import { simplifyString, kv2json } from "@app-utils/global-functions";
import { UserFilter, PageType } from "@app-utils/interfaces";


/**
 * custom pipes used by the app
 */


@Pipe({
  name: "emptyString"
})
export class PipeEmptyString implements PipeTransform {
  transform(input: any): boolean {
    /**
     * check wether an element is empty (matches regex /^\s*?$/)
     * @param input: the string to process
     * @returns: `true` if string is empty, `false` if not
     */
    let empty: boolean;
    const rgx = new RegExp("^\\s*$");
    try {
      if (input === undefined) {
        empty = true;
      } else {
        empty = rgx.test(String(input));
      }
    } catch(e) {
      empty = true;
    }
    return empty;
  }
}


@Pipe({
  name: "keyValToJson"
})
export class PipeKeyValToJson implements PipeTransform {
  transform(input: any): {} {
    /**
     * transform a string representation of a json of
     * a key-value-piped object into an object.
     * @param input: the string representation of the json 
     * @returns: the object built out of the string
     * @example: {key: "prenom", value: "laura"} => {prenom: "laura"}
     */
    input = JSON.parse(input);  // parse the json string as an object
    return kv2json(input);
  }
}


@Pipe({
  name: "stringContains"
})
export class PipeStringContains implements PipeTransform {
  transform(container: string, contained: string): boolean {
    /**
     * check wether `container` matches the user inputted `contained`:
     * simplify both strings (see `simplifyString()`) and check if 
     * `container` includes `contained`. this is used (for example) 
     * to filter the results displayed using a user's query string
     * @param container: the string that should contain `contained`
     * @param contained: the contained string
     * @returns: `true` if there's a match, `false` otherwise 
     */
     return ( simplifyString(container).includes(simplifyString(contained)) );
  }
}

@Pipe({
  name: "shortenString"
})
export class PipeShortenString implements PipeTransform {
  transform(input: string, stringLen: string|number = 30): string {
    /**
     * if a string is too long, it messes up the template.
     * if `input` is longer than `stringLen` chars, keep the `stringLen`
     * first characters + the following word
     * if there's a problem in the step above, don't keep the following 
     * word: only keep the first `stringlen` chars.
     * if the string is less than `stringLen` chars long, keep it as is.
     * @param input: the string to process
     * @param stringLen: the length of the string to be kept
     * @returns: the processed string
     */
    // if `stringLen` isn't a valid integer, only keep 30 chars
    try {
      Math.trunc(Number(stringLen));
    } catch {
      stringLen = 30;
    };
    if (typeof(stringLen) === "number" ){
      if (input.length > stringLen) {
        const shortened = input.slice(0, stringLen);
        const rgxMatch = input.slice(stringLen, input.length)
                              .match(/^\s*[^\s]+/g);
        if (rgxMatch) {
          input = `${shortened}${rgxMatch[0]}...`;
        } else {
          input = `${shortened}...`;
        }
      }
    }
    return input;
  }
}

@Pipe({
  name: "emptyUserFilter"
})
export class PipeEmptyUserFilter implements PipeTransform {
  transform(input: UserFilter): boolean {
    /**
     * check if a user filter object is empty (aka, if it corresponds
     * to all of the default values defined in the `UserFilter` class)
     * @param input: the input UserFilter object
     */
    let out: boolean = false;
    if ( input === new UserFilter() ) {
      out = true
    }
    return out;
  }
}

@Pipe({
  name: "resultMatchesUserFilter",
  pure: false  // causes optimisation issues, since the pipe is run extremely often
})
export class PipeResultMatchesUserFilter implements PipeTransform {
  transform (item: any, userFilter: UserFilter, pageType: PageType): boolean {
    /**
     * check if an item matches a user filter to decide wether to 
     * display it or not
     * @param item: the item to check
     * @param userFilter: the filter object by which to filter elements.
     * @param pageType: the type of page (Author, ItemIndex...) on which
     *                  we're working, to determine the kind of processing
     *                  necessary
     * @return: true if `item` matches `userFilter`
     */
    if (isEqual(userFilter, new UserFilter())) {
      // no custom filter has been defined
      var out: boolean = true;  
    } else {
      // define default variables and simplify comparison strings
      var out: boolean = false;
      var key: string = item.key;
      item = kv2json(item);
      userFilter.locationFilter = simplifyString(userFilter.locationFilter);
      userFilter.textFilter = simplifyString(userFilter.textFilter);
      
      // check if an item matches the user's filter.
      // different checking methods depending on the
      // page type and object type being checked (the
      // same filters aren't applied on an author and
      // on iconograhy)
      if (pageType === "itemIndex" || pageType === "authorMain") {
        // `userfilter.textFilter` filters the title and author names;
        // `userFilter.iiifOnly` is used and so is `userFilter.locationFilter`
        out = (
                ( simplifyString(item[key].title).includes(userFilter.textFilter) )
                || ( simplifyString(item[key].author_1).includes(userFilter.textFilter) )
                || ( simplifyString(item[key].author_2).includes(userFilter.textFilter) )
              ) && (
                (
                  userFilter.locationFilter === undefined 
                  || userFilter.locationFilter === "" 
                ) || simplifyString(item[key].title).includes(userFilter.locationFilter)
              ) && ( 
                userFilter.iiifOnly === false 
                || ( userFilter.iiifOnly === true &&  item[key].iiif_manifest === true )
              )
      } else if (pageType === "authorIndex") {
        out = ( simplifyString(key).includes(userFilter.textFilter) )
      }
    }
    return out
  }
  
}

@Pipe({
  name: "lastKeyVal"
})
export class PipeLastKeyVal implements PipeTransform {
  transform ( item: {}, keyValPair: { key: string|number, value: any } ): boolean {
    /**
     * check if the key-value pair `lastKeyValPair`
     * is the last one in the `item` object.
     * @param item: the item in which there's a key-value, in `keyValue` style
     * @param keyValPair: the pair to check, in `keyValue` style 
     * @returns: true if it is the last one, false else
     */
    const itemKeys: string[] = Object.keys(item);
    
    return itemKeys.indexOf(String(keyValPair.key)) === ( itemKeys.length - 1 )
  }
}
    

