import { NgModule } from '@angular/core';
import { RouterModule, Routes, TitleStrategy } from '@angular/router';

import { HomepageComponent } from "@app-pages/homepage/homepage.component";
import { ItemMainComponent } from "@app-pages/item-main/item-main.component";
import { ItemIndexComponent } from "@app-pages/item-index/item-index.component";
import { AuthorMainComponent } from "@app-pages/author-main/author-main.component";
import { AuthorIndexComponent } from "@app-pages/author-index/author-index.component";
import { NotFoundComponent } from "@app-pages/not-found/not-found.component";
import { BourseComponent } from "@app-pages/bourse/bourse.component";
import { MapMainComponent } from "@app-pages/map-main/map-main.component";

import { TitleTemplate } from "@app-services/title-template.service";


/**
 * module defining the routes
 */


const routes: Routes = [
    { path: "carte", component: MapMainComponent, title: "Carte"},
    { path: "Bourse", component: BourseComponent, title: "Bourse"},
    { path: "auteur/:id", component: AuthorMainComponent, title: "Auteur.ice"},
    { path: "auteur-index", component: AuthorIndexComponent, title: "Index des auteur.ice.s"},
    { path: "item/:id", component: ItemMainComponent, title: "Ressource iconographique" },
    { path: "item-index", component: ItemIndexComponent, title: "Index des ressources iconographiques" },
    { path: "", pathMatch: "full", component: HomepageComponent, title: "Accueil" },
    { path: "**", component: NotFoundComponent, title: "Page non trouvée"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: TitleStrategy, useClass: TitleTemplate }
  ]
})
export class AppRoutingModule { }
