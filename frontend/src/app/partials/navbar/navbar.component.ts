import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  sidebarVisible: boolean = false;
  
  ngOnInit(): void { }
  
  manageSidebar(): void {
    /**
     * manage the opening and closing of the sidebar
     * when clicking on the bruger button in the navbar
     * (`$(.navbar-burger-container)`). this function
     * handles several processes by adding/removing
     * classes:
     * - toggle `navbar-burger-horizontal` to `navbar-burger-cross`
     *   to change the style and animate the burger menu button
     * - toggle `this.sidebarVisible` to change the visibility
     *   of the sidebar
     * - when closing the sidebar, add a class `.to-delete`
     *   to the sidebar. this adds an animation on closing.
     *   once the animation has complete, `this.sidebarVisible`
     *   is toggled to false, which removes the navbar from the dom
     */
    if ( this.sidebarVisible === false ) {
      $(".navbar-burger-container").removeClass("navbar-burger-horizontal");
      $(".navbar-burger-container").addClass("navbar-burger-cross");
      this.sidebarVisible = true;
    } else {
      $("#sidebar").addClass("to-delete");
      $(".navbar-burger-container").removeClass("navbar-burger-cross");
      $(".navbar-burger-container").addClass("navbar-burger-horizontal");
      setTimeout( () => { 
        $("#sidebar").removeClass("to-delete");
        this.sidebarVisible = false; 
      }, 1000)
    }
  }
  
  closeSidebarOnEscape( ): void {
    /**
     * when pressing escape, if the side 
     * bar is opened, close it
     */
    if ( this.sidebarVisible ) {
      this.manageSidebar();
    }
  }
}
