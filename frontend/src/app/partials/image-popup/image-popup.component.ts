import { Component, Input } from '@angular/core';


@Component({
  selector: 'app-image-popup',
  templateUrl: './image-popup.component.html',
  styleUrls: ['./image-popup.component.css']
})
export class ImagePopupComponent {
  _popup: boolean = false;  // is a popup opened ?
  _popupImageUrl: string = ""  // what's the image url to display


  /**
   * retrieve the `_popupImageUrl` and `_popup`
   * variables from the parent template and pass 
   * them to this component's template 
   */
  get popupImageUrl(): string {
    return this._popupImageUrl;
  }
  @Input() set popupImageUrl(input: string) {
    this._popupImageUrl = input;
  }
  @Input() set popup(input: boolean) {
    this._popup = input;
  }
  get popup() {
    return this._popup
  }
}
