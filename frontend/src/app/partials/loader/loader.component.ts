import { Component, Input } from '@angular/core';


/**
 * a loader coded in CSS. the parent component
 * emits a `loaded` boolean value which is used
 * to toggle the visibility of the loader: when
 * `loaded`===false, the loader becomes 
 * invisible 
 */


@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent {
  @Input() loaded: boolean = false;  
}
