import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { ItemIndex } from "@app-utils/interfaces";
import { urlToStatic } from "@app-utils/global-functions";


/**
 * small thumbnail for a single item
 * on this component is decided:
 * - the background image to display
 * - the redirection to that item upon clicking it 
 */

@Component({
  selector: 'app-item-thumbnail',
  templateUrl: './item-thumbnail.component.html',
  styleUrls: ['./item-thumbnail.component.css']
})
export class ItemThumbnailComponent {
  db!: ItemIndex;
  id!: string;
  imageUrl: string = "";
  
  constructor( private router: Router ) { }
  
  /* getters and setters for the `_item` variable, used
     to send data to this component and pass it to the
     template */
  get item(): ItemIndex {
    /**
     * get the current item to send it to the template
     */
     return this.db;
  }
  @Input() set item(input: ItemIndex) {
    /**
     * define the `_item` variable,
     * the `_id` of the item and the
     * `imageUrl` by building a url 
     * to the image of that item
     * @param input: the data object
     */
    if (input) {
      this.db = input;
      this.id = Object.keys(this.db)[0];
      const fname: string = this.db[this.id].path;  // filename of the image
      if (fname !== "" && fname !== undefined) {
        this.imageUrl = urlToStatic(`images/${fname}`);
      }
    }
  }
  
  onSelect(): void {
    /**
     * upon selecting an item, redirect to the main page for that item
     */
    this.router.navigate([`/item/${this.id}`])
  }
}
