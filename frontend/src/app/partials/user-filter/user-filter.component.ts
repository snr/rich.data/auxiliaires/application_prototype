import { Component, Output, Input, EventEmitter, OnChanges } from '@angular/core';

import { UserFilter } from "@app-utils/interfaces";


/**
 * component to filter results based on a user's input. in this
 * item, the user's input is saved as variables and outputted to
 * the parent component, where actual filtering functions are
 * defined.
 *
 * all user filters as stored in a `UserFilter` object and passed
 * to the parent component where results are fitlered based on the
 * data stored in the `UserFilter` object
 *
 * this item defines: 
 * - a globally available search bar. typing in this search bar
 *   launches an event which outputs the text typed by the user
 *   to the parent component (`ItemIndex`, for example). in this
 *   parent component's template, a `*ngIf` condition will filter
 *   results based on the user's text
 * - a location-focus filter in the form of a dropdown menu. 
 *   this item allows to filter results by specific  locations, 
 *   such as `Rue Vivienne` and `Place de la Bourse`, to create
 *   geographic focuses. this filter isn't activated by default
 *   and must be activated with `[locationFilter]="true"` in the
 *   parent's component's template
 */
 
 
@Component({
  selector: 'app-user-filter',
  templateUrl: './user-filter.component.html',
  styleUrls: ['./user-filter.component.css']
})
export class UserFilterComponent implements OnChanges {
  
  constructor( ) { }
  
  /**
   * all `@Input()`s used in the parent component to activate 
   * or deactivate a filter
   */
  @Input() useLocationFilter: boolean = false;  // filter by a specific location
  @Input() useIiifFilter: boolean = false;  // filter by wether an image has, or not, a iiif filter

  /**
   * all `@Input()`s to get user inputted data, from which we'll build
   * our `userFilter` object
   */
  @Input() userFilter: UserFilter = new UserFilter();

  
  /**
   * @Output() communicates the `userFilter` object to
   * to the parent component using an `EventEmitter`. `userFilterEvent`
   * is accessible to the parent component using `(userFilterEvent)`
   * and the value passed to the parent can be updated using
   * `<child-component (userFilterEvent)="functionToProcessTheNewValue($event)">`
   *  (the value emitted by `userFilterEvent` is stored in `$event`)
   */
  @Output() userFilterEvent = new EventEmitter<UserFilter>();
    
  ngOnChanges(): void {
    this.emitUserFilter();
  }
  
  emitUserFilter(): void {
    /**
     * when the content of the `userFilter` changes, pass
     * this `content` to the parent component
     * 
     * what goes on in the template:
     * - `[(ngModel)]="userFilter"` binds template-side changes 
     *   on the `<input>` element to the `userFilter` typescript-side
     *   variable
     * - (ngModelChange)="emitUserFilter(userFilter)"` listens to changes
     *   on the above `ngModel` binding and activates the typescript-side
     *   `emitEventUser()` function
     */
    this.userFilterEvent.emit(this.userFilter);
  }
  
  resetUserFilter(): void {
    /**
     * when clicking on a button, reset all filters 
     * to their default value
     */
    this.userFilter = new UserFilter();
  }
}
