import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapPopupMainComponent } from './map-popup-main.component';

describe('MapPopupMainComponent', () => {
  let component: MapPopupMainComponent;
  let fixture: ComponentFixture<MapPopupMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapPopupMainComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapPopupMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
