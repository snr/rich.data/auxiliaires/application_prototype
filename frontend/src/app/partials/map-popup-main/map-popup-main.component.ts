import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as $ from "jquery";


@Component({
  selector: 'app-map-popup-main',
  templateUrl: './map-popup-main.component.html',
  styleUrls: ['./map-popup-main.component.css']
})
export class MapPopupMainComponent implements OnInit {
  popupContent = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur porta tempor dui, et viverra est fringilla nec. Aenean accumsan maximus dui eu gravida. Sed pulvinar eget ligula id porta. Quisque massa elit, accumsan eu tellus quis, dignissim aliquet felis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse arcu odio, sodales sed erat suscipit, scelerisque tempus urna. Morbi ultricies sodales libero nec rhoncus. Donec fringilla mauris sit amet turpis luctus, non elementum sem fringilla. Vivamus euismod erat quis neque venenatis, sit amet faucibus risus accumsan. Morbi tempus, elit sit amet porttitor molestie, odio mi congue nisi, lobortis ultricies purus arcu non elit. Nulla pharetra porttitor turpis et ullamcorper. Proin lacus mi, placerat in nunc non, posuere scelerisque lectus. Vestibulum pharetra luctus eleifend. Cras malesuada ligula vel sem semper, a faucibus turpis condimentum. Ut iaculis dictum libero, congue maximus nisl commodo sed. Integer sollicitudin nunc erat, lacinia consequat lectus aliquet vitae.";
  popupStyle?: { [key: string]: string|number };
  
  @Output() closePopupEvent = new EventEmitter<boolean>();
  
  ngOnInit() {
    this.positionPopup();
  }
  
  positionPopup(): void {
    const offset = $("#map-main").offset();
    this.popupStyle = {
      top: `${offset!.top}px`,
      left: `${offset!.left}px`,
    }
  }
  
  closePopup() {
    this.closePopupEvent.emit(false)
  }
}
