import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { AuthorIndex } from "@app-utils/interfaces";
import { urlToStatic } from "@app-utils/global-functions";


/**
 * small thumbnail on a single author.
 * on this component is decided:
 * - the background image to display
 * - the redirection to the author page when clicking the thumbnail
 */


@Component({
  selector: 'app-author-thumbnail',
  templateUrl: './author-thumbnail.component.html',
  styleUrls: ['./author-thumbnail.component.css']
})
export class AuthorThumbnailComponent {
  db!: AuthorIndex;
  id!: string;  // author's name
  imageUrl: string = "";
  
  constructor( private router: Router ) { }
  
  /* getters and setters for the `db` variable, used to send 
     data to this component and pass it to the template
  */
  get author(): AuthorIndex {
    /**
     * get the current author item and send it to the template 
     */
    return this.db;
  }
  @Input() set author(input: AuthorIndex) {
    /**
     * define the `db` variable, the `name` variable,
     * and the `imageUrl` by building an url to the page
     * to the image of that db
     * @param input: the data object
     */
    if (input) {
      this.db = input;
      this.id = Object.keys(this.db)[0];
      const fname: string = this.db[this.id];
      if (fname !== undefined && fname !== "") {
        // only add non-empty urls
        this.imageUrl = urlToStatic(`images/${fname}`);
      }
    }   
  }
  
  onSelect(): void {
    /**
     * when clicking on an autho's name, redirect to the main page for that item
     */
    this.router.navigate([`/auteur/${encodeURIComponent(this.id)}`]);
  }
}
