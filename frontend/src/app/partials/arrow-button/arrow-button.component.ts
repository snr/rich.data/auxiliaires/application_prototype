import { Component, Input, OnInit } from '@angular/core';

import { ScrollService } from "@app-services/scroll.service";
import { ComponentCoordinates } from "@app-utils/interfaces";


/**
 * large button to scroll between views
 */
@Component({
  selector: 'app-arrow-button',
  templateUrl: './arrow-button.component.html',
  styleUrls: ['./arrow-button.component.css']
})
export class ArrowButtonComponent implements OnInit {
  style!: {};
  visible!: boolean;
  @Input() currentViewName!: string;
  @Input() viewPositions!: ComponentCoordinates[];
  @Input() orientation!: "down"|"up"|"left"|"right";
  
  constructor( private scrollService: ScrollService ) { }
  
  ngOnInit(): void {
    /**
     * - orient the item depending on the 
     *  `this.orientation` input
     * - check that there's a next/prev item. 
     *   only display the button if there is one
     */
    // define the current view by its name    
    let currentView = this.scrollService.getCurrentViewByName(this.currentViewName, this.viewPositions);
    if ( this.orientation === "down" ) {
      this.style = { transform: "rotate(0deg)" };
      if ( 
        currentView !== undefined && 
        this.scrollService.findNext(currentView, this.viewPositions) !== currentView
      ) {
        // only show the button if there is a next item to scroll to
        this.visible = true;
      } else {
        this.visible = false;
      }
    } else if ( this.orientation === "left" ) {
      this.style = {  transform: "rotate(90deg)" }
    } else if ( this.orientation === "right" ) {
      this.style = { transform: "rotate(-90deg)" }
    } else if ( this.orientation === "up" ) {
      this.style = { transform: "rotate(180deg)" }
      if ( 
        currentView !== undefined && 
        this.scrollService.findPrev(currentView, this.viewPositions) !== currentView
      ) {
        // only show the button if there is a previous item to scroll to
        this.visible = true;
      } else {
        this.visible = false;
      }
    }
  }
}
