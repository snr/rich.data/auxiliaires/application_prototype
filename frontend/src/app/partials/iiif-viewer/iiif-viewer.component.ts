import { Component, NgZone, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import  OpenSeadragon  from "openseadragon";

import { IiifManifestProcessorService } from "@app-services/iiif-manifest-processor.service";


/**
 * a IIIF and image viewer using the OpenSeaDragon
 * image server
 * adapted from J.-C. Carius' code
 *
 * open sea dragon: 
 * - type definitions: https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/openseadragon/index.d.ts
 * - J.-C. Carius source: https://digital.inha.fr/visio-iiif/
 * - get started: https://openseadragon.github.io/docs/
 *
 * iiif:
 * - presentation manifest format: https://iiif.io/api/presentation/2.0/
 */


@Component({
  selector: 'app-iiif-viewer',
  templateUrl: './iiif-viewer.component.html',
  styleUrls: ['./iiif-viewer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IiifViewerComponent implements OnInit {
  viewer!: OpenSeadragon.Viewer;       // the iiif viewer
  tileSequence!: any[];                // source images
  iiifManifest!: {};                   // the manifest json
  _iiifManifestUrl!: string;           // the manifest url
  navImages!: OpenSeadragon.NavImages; // paths to images for the viewer's buttons
  options!: OpenSeadragon.Options;     // options for the viewer
  _iiifLoaded: boolean = false;        // has the iiif item finished loading ?

  /**
   * getters and setters for the IIIF manifest, used to create
   * the tileSequence object by parsing data from the manifest
   */
  @Input() set iiifManifestUrl(input: string) {
    /**
     * @param input: the url of the manifest
     */
    this._iiifManifestUrl = input;
  }
  get iiifManifestUrl() {
    return this._iiifManifestUrl;
  }
    
  /**
   * getters that toggle the `active` and `hidden`
   * classes on the two divs that contain the loader and the
   * iiif viewer.
   */
  set iiifLoaded(_in: boolean) {
    this._iiifLoaded = _in;
  }
  get iiifLoaded() {
    return this._iiifLoaded;
  }
  
  
  /**
   * output the `iiifLoaded` variable, indicating that the
   * iiif image has finished loading
   
  @Output() iiifLoadedEvent = new EventEmitter<boolean>();
  */
  
  constructor( 
    private ngZone: NgZone,
    private changeDetectorRef: ChangeDetectorRef,
    private iiifManifestProcessorService: IiifManifestProcessorService
  ) { }
  
  ngOnInit(): void {
    /**
     * initialize the OpenSeadragon viewer
     */
    this.initializeViewer();
  }
  
  initializeViewer(): void {
    /**
     * initialize the OpenSeadragon image viewer
     */
    this.iiifManifestProcessorService.processManifest(this.iiifManifestUrl).subscribe( res => {
      // get the data from the IIIF manifest
      this.tileSequence = res.sequence;
      
      // define the nav images
      this.navImages = {
        "zoomIn": {
          "REST": "zoomin_rest.png",
          "GROUP": "zoomin_grouphover.png",
          "HOVER": "zoomin_hover.png",
          "DOWN": "zoomin_pressed.png"
        },
        "zoomOut": {
          "REST": "zoomout_rest.png",
          "GROUP": "zoomout_grouphover.png",
          "HOVER": "zoomout_hover.png",
          "DOWN": "zoomout_pressed.png"
        },
        "home": {
          "REST": "home_rest.png",
          "GROUP": "home_grouphover.png",
          "HOVER": "home_hover.png",
          "DOWN": "home_pressed.png"
        },
        "fullpage": {
          "REST": "fullpage_rest.png",
          "GROUP": "fullpage_grouphover.png",
          "HOVER": "fullpage_hover.png",
          "DOWN": "fullpage_pressed.png"
        },
        "rotateleft": {
          "REST": "rotateleft_rest.png",
          "GROUP": "rotateleft_grouphover.png",
          "HOVER": "rotateleft_hover.png",
          "DOWN": "rotateleft_pressed.png"
        },
        "rotateright": {
          "REST": "rotateright_rest.png",
          "GROUP": "rotateright_grouphover.png",
          "HOVER": "rotateright_hover.png",
          "DOWN": "rotateright_pressed.png"
        },
        "flip": {
          "REST": "flip_rest.png",
          "GROUP": "flip_grouphover.png",
          "HOVER": "flip_hover.png",
          "DOWN": "flip_pressed.png"
        },
        "previous": {
          "REST": "previous_rest.png",
          "GROUP": "previous_grouphover.png",
          "HOVER": "previous_hover.png",
          "DOWN": "previous_pressed.png"
        },
        "next": {
          "REST": "next_rest.png",
          "GROUP": "next_grouphover.png",
          "HOVER": "next_hover.png",
          "DOWN": "next_pressed.png"
        }
      }
      
      // define the options
      this.options = {
        id: "openseadragon",
        prefixUrl: "/assets/openseadragon_icons/",
        tileSources: this.tileSequence,
        sequenceMode: true,
        initialPage: 0,
        navImages: this.navImages,
        showHomeControl: true,
        autoHideControls: true,
        viewportMargins: {
          top: 10,
          left: 10,
          right: 10,
          bottom: 10
        },
        showSequenceControl: this.tileSequence.length > 1,
        showNavigator: true,
        navigatorAutoFade: true,
        showRotationControl: true,
        // panHorizontal: true,
        // panVertical: true,
        
        // JCC's disabled options
        // navigationControlAnchor: 'ABSOLUTE',
        // fullPageButton: 'btn-plein-ecran',
        // zoomInButton: 'btn-zoom-plus',
        // zoomOutButton: 'btn-zoom-moins',
        // previousButton : 'btn-precedent',
        // nextButton: 'btn-suivant',
        // showReferenceStrip: true,
        
      };
      
      this.ngZone.runOutsideAngular(() => {
        // initialize the viewer
        this.viewer = OpenSeadragon(this.options)

        // define a boolean indicating that the iiif item has
        // finished loading. to do this, the `fully-loaded-change`
        // event must be fired INSIDE an `open` event.
        // see: https://github.com/openseadragon/openseadragon/pull/837#issuecomment-343233171
        this.viewer.addOnceHandler("open", () => {
          var tiledImage = this.viewer.world.getItemAt(0);
          tiledImage.addOnceHandler('fully-loaded-change', () => {
            this.iiifLoaded = tiledImage.getFullyLoaded();
            this.changeDetectorRef.detectChanges();  // explicitly update the view
          });
        })
      });
    });
  }
  
  /*emitIiifLoaded(): void {
    /**
     * emit a boolean value indicating the
     * iiif viewer has finished loading
     *    
    this.iiifLoadedEvent.emit(this.iiifLoaded);
  }*/
}
