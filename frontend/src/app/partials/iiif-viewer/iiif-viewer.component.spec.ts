import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IiifViewerComponent } from './iiif-viewer.component';

describe('IiifViewerComponent', () => {
  let component: IiifViewerComponent;
  let fixture: ComponentFixture<IiifViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IiifViewerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IiifViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
