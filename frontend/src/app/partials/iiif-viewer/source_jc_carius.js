 var Visionneuse = false 
     , Json
     , Sequence = [] // tile source
     , NavImages
     , Parcours
     , Annot = [];

$(document).ready(function(){
  var manifeste = getUrlVars()['manifeste'];
  var image = getUrlVars()['image'];

  if(manifeste){

    /**
     * launch ajax request: fetch the IIIF manifest,
     * - parse it to extract the url of to the image file (?),
     *   use it to build an `image` variable containing a single
     *   source tile in json and apped this `image` to `Sequence`
     * - parse the manifest to extract the annotations
     */
    $.ajax({
      url:manifeste,
      success: function(json){
        Json = json;

        $.each(Json.sequences[0].canvases,function(n,c){
          var src = c.images[0].resource['@id'];
          src = src.replace(/\/full\/[,\d]+\//,'/full/full/');
          $.each(c.metadata,function(n,a){
            if(a.label == 'annotates'){
              var j = a.value.replace(/&quot;/g,'"');
              Annot.push(JSON.parse(j));
            }
          })
          var image = { type: 'image', url: src };
          Sequence.push(image);
        })

        InitVisionneuse();

      }
    })

  }

  if(image){

    Sequence.push({ type: 'image', url: image});
    InitVisionneuse();
  }

})

function InitVisionneuse(){
  /**
   * initialize and configure the Visionneuse
   */

  Visionneuse = OpenSeadragon({
      id: "visio",
      prefixUrl: "https://digital.inha.fr/visio-iiif/images/",
      tileSources: Sequence,
      sequenceMode: true,
      initialPage: 0 ,
      navImages: NavImages,
      showHomeControl: false,
      autoHideControls: true,
      viewportMargins: {
                top: 0,
                left: 0,
                right: 0,
                bottom: 0

            },
      showSequenceControl: Sequence.length > 1
      //navigationControlAnchor: 'ABSOLUTE',
      /*fullPageButton: 'btn-plein-ecran',
      zoomInButton: 'btn-zoom-plus',
      zoomOutButton: 'btn-zoom-moins',
      previousButton : 'btn-precedent',
      nextButton: 'btn-suivant',*/
      //showReferenceStrip: true,
  });

  Parcours = OpenSeadragon.Annotorious(Visionneuse);
  Parcours.disableEditor = true;
  Parcours.readOnly = true;
  Parcours.disableSelect = true;
  //Parcours.selectAnnotation(Annot[0].id);

  $('.spinner-border').hide();
  $('img[alt="Previous page"]').css('margin-left','20px')
  if(Sequence.length < 2 ){
    $('img[alt="Previous page"],img[alt="Next page"]').hide();
  }else{
    $('.leg').text('1/'+Sequence.length);
  }

  $('img[alt="Toggle full page"]').each(function(){
    $(this).parent().attr('title','Plein Ã©cran').attr('id','btn-plein-ecran').addClass('bouton')
  });
  $('img[alt="Zoom out"]').each(function(){
    $(this).parent().attr('title','Zoom arriÃ¨re').attr('id','btn-zoom-moins').addClass('bouton')
  });
  $('img[alt="Zoom in"]').each(function(){
    $(this).parent().attr('title','Zoom avant').attr('id','btn-zoom-plus').addClass('bouton')
  });
  $('img[alt="Next page"]').each(function(){
    $(this).parent().attr('title','Image suivante').attr('id','btn-suivant').addClass('bouton')
  });
  $('img[alt="Previous page"]').each(function(){
    $(this).parent().attr('title','Image prÃ©cedente').attr('id','btn-precedent').addClass('bouton')
  });

  $('.bouton').parent().addClass('bloc').parent().addClass('bloc').parent().addClass('menu');


  Visionneuse.addHandler("page", ChangementPage, {});

  var config = $('.config');

  Visionneuse.addHandler("pre-full-screen", function(){ console.log('pre-full-screen'); });
  Visionneuse.addHandler("full-screen", ChangementPleinEcran, {pageCourante: Visionneuse.currentPage()});
  Visionneuse.addHandler("pre-full-page", function(){ console.log('pre-full-page'); });
  Visionneuse.addHandler("full-page", function(){ console.log('full-page'); });

  var isFullyLoaded = false;

  Visionneuse.world.addHandler('add-item', function(event) {
    var tiledImage = event.item;
    tiledImage.addHandler('fully-loaded-change', function() {
      var newFullyLoaded = areAllFullyLoaded();
      if (newFullyLoaded !== isFullyLoaded) {
        isFullyLoaded = newFullyLoaded;
      }
      $('.spinner-border').hide();
      $('.leg').text((Visionneuse.currentPage()+1)+'/'+Sequence.length);
      if(Annot.length){
        Parcours.setAnnotations(Annot);
        Parcours.fitBoundsWithConstraints(Annot[0]);
        $('.a9s-annotationlayer > g').tooltip({
          title: function(){
            var a = Annot[Visionneuse.currentPage()].body[0].value
            return a;
          },
          placement: 'bottom',
          html: true,
        });
        $('.a9s-annotationlayer > g').tooltip('show');
        var mode = getUrlVars()['mode'];
        console.log(mode);
        if(mode == '2'){
          $('.a9s-annotationlayer').addClass('mode-2')
        }
      }

    });
  });

  function ChangementPage(event, obj){
    var courant = event.page;
    $('.spinner-border').show();
  }

  function ChangementPleinEcran(eventSource, fullScreen, userData){
    console.log('full-screen');
    console.log(config);
    config.appendTo('body')
  }

  function areAllFullyLoaded() {
    var tiledImage;
    var count = Visionneuse.world.getItemCount();
    for (var i = 0; i < count; i++) {
      tiledImage = Visionneuse.world.getItemAt(i);
      if (!tiledImage.getFullyLoaded()) {
        return false;
      }
    }
    return true;
  }

  Visionneuse.addHandler('animation-finish', finAnimation, {} );

  function finAnimation(event, obj){
    $('.a9s-annotationlayer > g').tooltip('show');
  }

  Visionneuse.addHandler('animation-start', debutAnimation, {} );

  function debutAnimation(event, obj){
    $('.a9s-annotationlayer > g').tooltip('hide');
  }


}

function getUrlVars(){
    /**
     * extract the iiif manifest from a url
     */
    var vars = [], hash;
    var hashes = window.location.search.slice(window.location.search.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


/**
 * images for the buttons in the viewer
 */
NavImages = {
  "zoomIn": {
    "REST": "plus.png",
    "GROUP": "plus.png",
    "HOVER": "plus.png",
    "DOWN": "plus.png"
  },
  "zoomOut": {
    "REST": "moins.png",
    "GROUP": "moins.png",
    "HOVER": "moins.png",
    "DOWN": "moins.png"
  },
  "home": {
    "REST": "home.png",
    "GROUP": "home.png",
    "HOVER": "home.png",
    "DOWN": "home.png"
  },
  "fullpage": {
    "REST": "plein-ecran.png",
    "GROUP": "plein-ecran.png",
    "HOVER": "plein-ecran.png",
    "DOWN": "plein-ecran.png"
  },
  "rotateleft": {
    "REST": "rotateleft_rest.png",
    "GROUP": "rotateleft_grouphover.png",
    "HOVER": "rotateleft_hover.png",
    "DOWN": "rotateleft_pressed.png"
  },
  "rotateright": {
    "REST": "rotateright_rest.png",
    "GROUP": "rotateright_grouphover.png",
    "HOVER": "rotateright_hover.png",
    "DOWN": "rotateright_pressed.png"
  },
  "flip": {
    "REST": "flip_rest.png",
    "GROUP": "flip_grouphover.png",
    "HOVER": "flip_hover.png",
    "DOWN": "flip_pressed.png"
  },
  "previous": {
    "REST": "precedent.png",
    "GROUP": "precedent.png",
    "HOVER": "precedent.png",
    "DOWN": "precedent.png"
  },
  "next": {
    "REST": "suivant.png",
    "GROUP": "suivant.png",
    "HOVER": "suivant.png",
    "DOWN": "suivant.png"
  }
}

