import { Component } from '@angular/core';
import { Router } from "@angular/router";


/**
 * container for the whole app 
 */
 
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Richelieu. Histoire du quartier';
  
  constructor( private router: Router ) { }
  
  isIntro() {
    /**
     * to toggle the display of the navbar.
     * `true` if we're on the home page, false else.
     * if `true`, the navbar isn't shown and will be
     * handled by the `homepage` component
     */
    return this.router.url === "/";
  }
}
