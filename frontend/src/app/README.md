# structure du dossier `app/`

ce dossier a la structure suivante:

```
root/
  |__pages/ : composants représentant des pages principales, 
  |           accessibles à l'aide d'un url
  |__partials/ : composants inclus à l'intérieur d'une autre page,
  |              qui ne sont pas accessibles directement
  |__serivces/ : l'ensemble des services utilisés par le front-end
  |__utils/ : éléments utilitaires: fonctions et variables globales,
              interfaces et types de données utilisés par l'application
```


