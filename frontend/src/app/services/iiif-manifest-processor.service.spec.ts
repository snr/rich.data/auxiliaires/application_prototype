import { TestBed } from '@angular/core/testing';

import { IiifManifestProcessorService } from './iiif-manifest-processor.service';

describe('IiifManifestProcessorService', () => {
  let service: IiifManifestProcessorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IiifManifestProcessorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
