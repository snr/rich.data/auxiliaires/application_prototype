import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"; 
import { Observable } from "rxjs";

import { Author, AuthorIndex, Item, ItemIndex } from "@app-utils/interfaces";
import { BACKEND_URL } from "@app-utils/constants";


/**
 * interaction with the backend database
 *
 * this service handles all interactions with the backend
 * by consuming the middleware API. it passes the data
 * retrieved to other components
 */


@Injectable({
  providedIn: 'root'
})
export class BackendInteractionService {

  constructor( private httpClient: HttpClient ) { }
  
  /**
   *
   ****************************
   * POSSIBILITY OF EVOLUTION *
   ****************************
   * 
   * later on, the backend structure may/will change.
   * to keep consistency once stable and efficient 
   * front-end interfaces are defined, if the data
   * sent in the backend needs to be processed to
   * fit the interfaces, subscribe to the `httpClient`,
   * pipe the response, transform it into something
   * that matches with the interface and return it
   * as an observable:
   * this.httpClient.get(...).pipe(map(
   *    ... 
   * ))
   *
   * https://github.com/catbar-odhn/catbar_app/blob/main/src/app/services/db-artist-index.service.ts
   *
   */
  
  authorMain(_id: string): Observable<Author> {
    /**
     * filter the database to fetch an `Author` 
     * object with all works by a single author
     * @param _id: the identifier of the author 
     *             for which to retrieve data
     */
    return this.httpClient
               .get(`${BACKEND_URL}/mw/author-main/${_id}`) as Observable<Author>
  }
  
  authorIndex(): Observable<AuthorIndex> {
    /**
     * filter the database to fetch an `AuthorIndex` 
     * authors in the database
     */
    return this.httpClient
               .get(`${BACKEND_URL}/mw/author-index`) as Observable<AuthorIndex>
  }
  
  itemMain(_id: string): Observable<Item> {
    /**
     * fetch a single item from the backend
     * based on its `_id` identifier
     */
    return this.httpClient.get(`${BACKEND_URL}/mw/item-main/${_id}`) as Observable<Item>
  }
  
  itemIndex(): Observable<ItemIndex> {
    /**
     * fetch the complete database to get an 
     * index of all items
     */
    // `as Interface` allows to force type on an api response 
    return this.httpClient
               .get(`${BACKEND_URL}/mw/item-index`) as Observable<ItemIndex>;
  }
}