import { Injectable } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { TitleStrategy, RouterStateSnapshot } from '@angular/router';


/**
 * a template to define custom titles displayed in the page's header.
 * the titles are following the pattern: `RICH.DATA | ${title}`
 */


@Injectable({
  providedIn: "root"
})

export class TitleTemplate extends TitleStrategy {
  constructor(private readonly title: Title){
    super();
  }
  override updateTitle(routerState: RouterStateSnapshot) {
    /*
      define a custom title for the headers
    */
    const title = this.buildTitle(routerState);
    if (title !== undefined) {
      this.title.setTitle(`Richelieu. Histoire du quartier | ${title}`);
    }
  }
}