import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import * as L from "leaflet";
import * as $ from "jquery";

import { webMapRootUrl, webMapRootUrlKey } from "@app-utils/interfaces";


/**
 * utils and functions for leaflet webmaps.
 * 
 * this service does NOT handle the interaction
 * with backend databases to fetch data for the maps.
 * all database interaction should be contained in
 * `backend-interaction.service` 
 */


@Injectable({
  providedIn: 'root'
})
export class MapService {
  rootUrls: webMapRootUrl = {
    ignWms: "https://wxs.ign.fr/cartes/geoportail/r/wms",
    ignWmts: "https://wxs.ign.fr/cartes/geoportail/wmts"
  }

  constructor() { }
  
  
  /*********************************************
   *          create new map elements          *  
   *********************************************/
  buildBackgroundTiles(
    // richelieuZone  TODO
  ) {
    /**
     * define all the background map tiles + geojson borders
     * of the neighbourhood
     * 
     * // TODO @param {geoJSON, object} richelieuZone: the geoJson of the neighbourhood's borders 
     */
    return [
      L.tileLayer(
        // base stamen tile
        "https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png", {
          pane: "basePane",
          minZoom: 3,
          maxZoom: 18,
          attribution: `Fonds de cartes par <a href='http://stamen.com'>` +
            `Stamen Design</a>, ` +
            `<a href='http://creativecommons.org/licenses/by/3.0'>` +
            `CC BY 3.0</a>`
        }
      ), L.layerGroup([
        // the 2 État Major 1820-1866 tiles used, to be 
        // able to process them as a single laye 
        this.buildWmtsTile(
          "ignWmts", "GEOGRAPHICALGRIDSYSTEMS.ETATMAJOR40", "bgPane", 7, 15.5
        ), this.buildWmsTile(
          "ignWms", "GEOGRAPHICALGRIDSYSTEMS.ETATMAJOR40", "bgPane", 15.5, 17
        )
      ]), this.buildWmtsTile(
        // the 1906 map
        "ignWmts", "GEOGRAPHICALGRIDSYSTEMS.1900TYPEMAPS", "bgPane", 10, 15.5
      ), L.tileLayer(
        // alpage-vasserot
        "https://tile.maps.huma-num.fr/uc2usU/d/Alpage_Vasserot_1830/{z}/{x}/{y}.png", {
          pane: "bgPane"
          , opacity: 0.85
          , minZoom: 14
        }
      ),L.tileLayer(
        // plan 1900
        "https://tile.maps.huma-num.fr/uc2usU/d/MOSA_1900_PARIS/{z}/{x}/{y}.png", {
          pane: "bgPane"
          , opacity: 0.85
        }
      )
      // TODO
      // , L.geoJSON(
      //    // the borders of the neighbourhood
      //    richelieuZone, style = {
      //      color: "var(--cs-plum)"
      //    }, pane = "basePane", interactive = false
      // )
    ]
  }
  
  buildWmtsTile(
    baseUrl: webMapRootUrlKey, layer: string, pane: string|undefined = undefined, 
    minZoom: number = 7, maxZoom: number = 16
  ): L.TileLayer {
  /**
   * build a WMS tile (used by the IGN to serve custom maps)
   * using the WMTS protocol
   * based on this tutorial: https://geoservices.ign.fr/documentation/services/utilisation-web/affichage-wmts/leaflet-et-wmts
   * 
   * @param baseUrl: the key pointing to the base url of the WMTS server (see `this.rootUrls`).
   * @param layer: the name of the layer
   * @param pane: the pane a layer belongs to. defaults to `tilePane`, the default leaflet value
   * @param minZoom: the minimum zoom
   * @param maxZoom: the maximum zoom
   */
    return L.tileLayer(
      `${this.rootUrls[baseUrl]}?` 
      + "&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0" 
      + "&STYLE=normal" 
      + "&TILEMATRIXSET=PM" 
      + "&FORMAT=image/jpeg" 
      + `&LAYER=${layer}` 
      + "&TILEMATRIX={z}" 
      + "&TILEROW={y}" 
      + "&TILECOL={x}",
      {
        pane: pane !== undefined && pane !== "" ? pane : "tilePane",  // if no pane is defined, add the default value
        minZoom : minZoom,
        maxZoom : maxZoom,
        attribution : "<a href='https://www.geoportail.gouv.fr/'>IGN-F/Geoportail</a>",
        tileSize : 256 // les tuiles du Géooportail font 256x256px
      }
    )
  }
  
  buildWmsTile(
    baseUrl: webMapRootUrlKey, layer: string, pane: string|undefined = undefined, 
    minZoom: number = 7, maxZoom: number = 16
  ): L.TileLayer {
    /**
     * build a WMS tile layer (used by the IGN to serve custom maps)
     * based on this tutorial: https://leafletjs.com/examples/wms/wms.html
     *
     * @param baseUrl: the key pointing to the base url of the WMS server (see `this.rootUrls`).
     * @param layer: the name of the layer
     * @param pane: the pane a layer belongs to. defaults to `tilePane`, the default leaflet value
     * @param minZoom: the minimum zoom
     * @param maxZoom: the maximum zoom
     */
    return L.tileLayer.wms(
      `${this.rootUrls[baseUrl]}?`,
      {
        pane: pane !== undefined && pane !== "" ? pane : "tilePane",  // if no pane is defined, add the default value
        layers: layer,
        minZoom : minZoom,
        maxZoom : maxZoom,
        version: "1.3.0",
        uppercase: true,
        attribution : "<a href='https://www.geoportail.gouv.fr/'>IGN-F/Geoportail</a>",
      }
    )
  }
  
  buildImageOverlayPopup( imageOverlay: L.ImageOverlay ): L.Layer {
    /**
     * create a layer on which will be added a popup for an 
     * `imageOverlay` object.
     * for an `imageOverlay` to have a popup, it must be interactive
     * (`interactive: true` in the layer's options). this gives an
     * ugly zooming animation effet to the `imageOverlay`.
     * => our solution: we create a new transparent layer 
     * that fits the bounds of `imageOverlay`. the popup will
     * be placed on that layer.
     *
     * @param imageOverlay: the image layer on which to place 
     *                      the new layer with popup
     */
    const bounds: L.LatLngBounds = imageOverlay.getBounds()
    const shape = [
      bounds.getNorthWest()
      , bounds.getSouthWest()
      , bounds.getSouthEast()
      , bounds.getNorthEast()
    ]
    return new L.Polygon(shape, { opacity: 0, fillOpacity: 0 })
  }
  
  
  /*********************************************
   *   modify the map object (not the layers)  *  
   *********************************************/
  setBounds(layerGroup: L.FeatureGroup, map: L.Map) {
    /**
     * set the bounds of a map based on the bounds 
     * in `layerGroup` group of layers to center on 
     * the layers in this group
     */
    if (layerGroup.getLayers().length) {
      map.fitBounds(layerGroup.getBounds());
    }
  }
  
  
  /*********************************************
   *                   utils                   *  
   *********************************************/
  getWidths(): number[] {
    /**
     * calculate useful map widths:
     * - the width of a popup element
     * - the width of the map
     *
     * **************** WARNING **************** 
     * there may be a potential error where we 
     * calculate the map's width and height
     * before having loaded it (not sure tho). 
     * to avoid a potential error, we may need to
     *  wait for the full page to have loaded before 
     * getting the widths. see commented code below
     */
    /* return new Observable( (subscriber) => {
      $(document).on("ready", () => {
        subscriber.next( [ 
          $(".map-popup").width() || 0
          , $("#map-main").height() || 0
          , $("#map-main").width() || 0
        ] )
      })
    }) */
    return [ 
      $(".map-popup").width() || 0
      , $("#map-main").height() || 0
      , $("#map-main").width() || 0
    ]
  }
  
  repositionMap(
    map: L.Map
    , layer: L.ImageOverlay|undefined
    , popupDisplayed: boolean
    , mapWidth: number
  ): void {
    /**
     * reposition the map when opening/closing a popup
     * to center it on a layer, so that the layer is
     * always visible and never hidden behind a popup
     *
     * @param map: the map we're working on
     * @param layer: the layer that should be visible 
     *               (the one linked to the clicked popup)
     * @param popupDisplayed: whether the popup is displayed or not
     * @param mapWidth: the map's width
     */
    if ( layer !== undefined ) {
      map.fitBounds( layer.getBounds()
        , {
          paddingTopLeft: popupDisplayed
                          ? L.point( 0.1 * mapWidth, 0 ) 
                          : L.point( - 0.1 * mapWidth, 0 )
        }
      )
    }
  }
  
  
}
