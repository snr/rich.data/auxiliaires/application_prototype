import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"; 
import { map } from "rxjs/operators";
import { Observable } from "rxjs";

/**
 * from a url pointing to a IIIF manifest, retrieve the
 * manifest and process it to extract all necessary data
 * to create a IIIF viewer
 */
 
 
@Injectable({
  providedIn: 'root'
})
export class IiifManifestProcessorService {
  iiifManifest?: any;  // should implement a iiif manifest type: search "iiif" on the definitelytyped homepage

  constructor( private httpClient: HttpClient ) { }
  
  processManifest(manifestUrl: string): Observable<any> {
    /**
     * process a manifest to extract relevant data
     */
    let out: {
      sequence: Object[]
    } = {sequence: []};  // output format: variables mapped to the data they contain
    
    // `{observe: "response"}`: read the full response object, not just the body
    // if i do `.subscribe( res => {...}, err => {...})`, errors will be caught
    // in `err`
    return this.httpClient
               .get(manifestUrl, {observe: 'response'})
               .pipe(map( res => {
      if (res.ok == true && res.body) {
        // parse the iiif manifest to extract the tiles
        this.iiifManifest = res.body;
        this.iiifManifest.sequences[0].canvases.forEach((c: any) => {
          var url: string = c.images[0]
                                    ["resource"]
                                    ["@id"]
                                    .replace(/\/full\/[,\d]+\//, "/full/full/");  // get full image
          var sequenceTile = {"type": "image", url: url, "height": 1};
          out.sequence.push(sequenceTile);
          
          /**
           * WHERE WE AT: WE ADDED THE TILES
           */
          
        })
      }
      return out;
    }))
  }
}
