import { Injectable } from '@angular/core';
// import isEqual from 'lodash.isequal';
// import { Observable } from "rxjs";

import { ComponentCoordinates } from "@app-utils/interfaces";
import { getFirstKey } from "@app-utils/global-functions";


/**
 * service to handle the scrolling to other elements on the page
 * and to interact with `ComponentCoordinates` objects
 */


@Injectable({
  providedIn: 'root'
})
export class ScrollService {
  // viewPositions: ComponentCoordinates = {};  // array of positions of the different views, for the scroll service
  // preScrollPosition: number = 0;  // the pre-scroll position to determine if we're scrolling up or down
  // postScrollPosition: number = 0  // the position after the scroll
  
  constructor() { }
  
  reorderViewPositions(
    viewPositions: ComponentCoordinates[]
  ): ComponentCoordinates[] {
    /**
     * reorder the different view positions by their value in `top`
     */
    let out: ComponentCoordinates[] = [];
    let positions: number[] = []
      
    // build an array of top positions by which to order the dict  
    for ( let v = 0; v < viewPositions.length; v++ ) {
      let current = viewPositions[v]
      positions.push(current.top);
    }
      
    // order the positions
    positions.sort(function(a, b) { return a < b ? -1 : a > b ? 1 : 0; });
      
    // reorder `viewPositions` based on their respective positions: 
    // items with a bigger `top` are lower down the html page and
    // are placed at the end.
    for ( let p = 0; p < positions.length; p++ ) {
      for ( let v = 0; v < viewPositions.length; v++ ) {
        let key: string = getFirstKey(viewPositions[v])
        let current = viewPositions[v];
        if ( current.top === positions[p] ) {
          out.push(current);
        }
      }
    }
      
    // return
    return out;
  }
  
  getCurrentViewByName( 
    currentViewName: string, 
    viewPositions: ComponentCoordinates[] 
  ): ComponentCoordinates|undefined {
    /**
     * find the current view by its name
     * @param currentViewName: the name of the view
     * @param viewPositions: the view positionbs
     * @returns: the `ComponentCoordinates` corresponding
     *           to `currentViewName` if it is found, 
     *           `undefined` else
     */
    let currentView: ComponentCoordinates|undefined = undefined;
    for ( let i=0; i < viewPositions.length; i++ ) {
      if ( currentViewName === viewPositions[i].name ) {
        currentView = viewPositions[i];
        break;
      }
    }
    return currentView
  }
  
  getCurrentViewIndex(
    currentView: ComponentCoordinates,
    viewPositions: ComponentCoordinates[] 
  ): number {
    /**
     * get the index of `currentView` in the
     * `viewPositions` array
     */
    let currentViewIndex: number = -1;
    for ( let i=0; i < viewPositions.length; i++ ) {
      if ( currentView.name === viewPositions[i].name ) {
        currentViewIndex = i;
        break
      }
    }
    return currentViewIndex;
  }
  
  findPrev(
    currentView: ComponentCoordinates,
    viewPositions: ComponentCoordinates[] 
  ): ComponentCoordinates {
    /**
     * find the previous item in `viewPositions`
     * relative to `currentView`. basically a simplified
     * version of `findTarget()`
     * @param currentView: the current view (item visible on the viewer)
     * @param viewPositions: the different views and their positions
     * @returns: the next view position as an observable
     */
    // get the index of the current view
    const currentViewIndex: number = this.getCurrentViewIndex(currentView, viewPositions)
    // find the previous item, if it exists
    let targetView: ComponentCoordinates = currentView;
    if ( (currentViewIndex - 1) >= 0 ) {
      // if there's a previous item, select it
      targetView = viewPositions[ currentViewIndex - 1 ]
    }
    return targetView;
  }
  
  findNext(
    currentView: ComponentCoordinates,
    viewPositions: ComponentCoordinates[] 
  ): ComponentCoordinates {
    /**
     * find the next item in `viewPositions`
     * relative to `currentView`. basically a simplified
     * version of `findTarget()`
     * @param currentView: the current view (item visible on the viewer)
     * @param viewPositions: the different views and their positions
     * @returns: the next view position as an observable     
     */
    // get the index of the current view
    const currentViewIndex: number = this.getCurrentViewIndex(currentView, viewPositions)
    // find the previous item, if it exists
    let targetView: ComponentCoordinates = currentView;
    if ( (currentViewIndex + 1) < viewPositions.length ) {
      // if there's a next item, select it
      targetView = viewPositions[ currentViewIndex + 1 ]
    }
    return targetView;
  }
  
  findTarget(
    currentView: ComponentCoordinates,
    postScrollPosition: number, 
    viewPositions: ComponentCoordinates[]
  ): ComponentCoordinates {
    /**
     * calculate if/where to scroll: find the target to which
     * we need to scroll
     * @param currentView: the current view, used to calculate the pre-scroll position
     * @param postScrollPosition: the position after scrolling
     * @param viewPositions: the different views and their positions
     * @returns: the name of the view to which to scroll
     */
    let targetView: ComponentCoordinates = currentView;  // by default, no need to scroll to another element: the element to scroll to is the same as the current element
    const minScroll: number = 0;  // minimum number of pixels by which we need to scroll
    const preScrollPosition: number = currentView.top;  // the position of the previous view
       
    // get the index of the current view in the `viewPositions` array
    const currentViewIndex = this.getCurrentViewIndex(currentView, viewPositions)
    
    // possible problem if `preScrollPosition` or `postScrollPosition` are === 0
    if ( Math.abs(postScrollPosition - preScrollPosition) > minScroll ) {
          
      if ( preScrollPosition < postScrollPosition ) {
        // down scroll
        if ( (currentViewIndex + 1) < viewPositions.length ) {
          // if there is a next item in the `viewPositions` list, assign it to `targetView`
          targetView = viewPositions[ currentViewIndex + 1 ]
        }  
      } else if ( preScrollPosition > postScrollPosition ) {
        // up scroll
        if ( (currentViewIndex - 1) >= 0 ) {
          // if there's a previous item, toggle it
          targetView = viewPositions[ currentViewIndex - 1 ]
        }
      } else {
        targetView = currentView;
      }
    }
    return targetView;
  }
}
