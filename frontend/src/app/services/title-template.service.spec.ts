import { TestBed } from '@angular/core/testing';

import { TitleTemplateService } from './title-template.service';

describe('TitleTemplateService', () => {
  let service: TitleTemplateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TitleTemplateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
