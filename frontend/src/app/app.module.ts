import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomepageComponent } from '@app-pages/homepage/homepage.component';
import { ItemMainComponent } from '@app-pages/item-main/item-main.component';
import { ItemIndexComponent } from '@app-pages/item-index/item-index.component';
import { ItemThumbnailComponent } from '@app-partials/item-thumbnail/item-thumbnail.component';
import { ImagePopupComponent } from '@app-partials/image-popup/image-popup.component';
import { AuthorIndexComponent } from '@app-pages/author-index/author-index.component';
import { AuthorMainComponent } from '@app-pages/author-main/author-main.component';
import { NotFoundComponent } from '@app-pages/not-found/not-found.component';
import { AuthorThumbnailComponent } from '@app-partials/author-thumbnail/author-thumbnail.component';
import { UserFilterComponent } from '@app-partials/user-filter/user-filter.component';
import { IiifViewerComponent } from '@app-partials/iiif-viewer/iiif-viewer.component';
import { BourseComponent } from '@app-pages/bourse/bourse.component';
import { LoaderComponent } from '@app-partials/loader/loader.component';
import { NavbarComponent } from '@app-partials/navbar/navbar.component';
import { ArrowButtonComponent } from '@app-partials/arrow-button/arrow-button.component';
import { MapMainComponent } from '@app-pages/map-main/map-main.component';
import { MapPopupMainComponent } from '@app-partials/map-popup-main/map-popup-main.component';


import { 
  PipeEmptyString, PipeKeyValToJson, PipeStringContains
  , PipeShortenString, PipeEmptyUserFilter, PipeResultMatchesUserFilter
  , PipeLastKeyVal
} from "@app-utils/pipes";
import { FooterComponent } from './partials/footer/footer.component';


@NgModule({
  declarations: [
    /* components */
    AppComponent,
    HomepageComponent,
    ItemMainComponent,
    ItemIndexComponent,
    ItemThumbnailComponent,
    ImagePopupComponent,
    AuthorIndexComponent,
    AuthorMainComponent,
    NotFoundComponent,
    AuthorThumbnailComponent,
    UserFilterComponent,
    IiifViewerComponent,
    BourseComponent,
    LoaderComponent,
    NavbarComponent,
    ArrowButtonComponent,
    MapMainComponent,
    MapPopupMainComponent,
    
    /* pipes */
    PipeEmptyString,
    PipeKeyValToJson,
    PipeStringContains,
    PipeShortenString,
    PipeEmptyUserFilter,
    PipeResultMatchesUserFilter,
    PipeLastKeyVal,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
