$(document).ready( () => {
  // buildRotatingBanner();
  const img = defineImg();
  var clicked = false;
  var imgStyleSave = { }; /* used to save the style of elements. 
    structure:
    ```
    {
      element-id : {
        css-property: css-value,  
      }
    }
    ```
    fill it with `saveStyle()` and empty it
    with `emptyStyle()`
  */
  
  function defineImg() {
    /**
     * on document load, give a unique identifier
     * to all elements that match `$("#img-container > img")`
     * and define the `img` variable
     */
    for ( let i=0; i < $("#img-container > img").length; i++ ) {
      identifyEl($("#img-container > img")[i]);
    }
    return $("#img-container > img");
  }
  
  function identifyEl(el) {
    /**
     * check if element `el` has an html id.
     * if not, add a random id to the html element.
     *
     * warning: element must be parsed as jquery using
     * `$(el)`, while it's not the case elsewhere. dunno why
     *
     * @returns: the element's id (wether it's
     * been created now or it existed before)
     */
    if ( $(el).attr("id") === undefined ) {
      $(el).attr("id", crypto.randomUUID());
    }
    return $(el).attr("id");
  }
  
  
  function getNoFromCss(a) {
    /**
     * ***elle est sale cette fonction qd même***
     *
     * extract a number from a css value:
     * `-90.9px` => `-90.9`.
     * @param a: the css value we want to process
     * @returns: the extracted number as a `Number`,
     *           to allow for calculations
     */
    return Number( a.replace(/[a-zA-Z]+/g, "") )
  }
  
  
  function saveStyle(el, pr) {
    /**
     * save a style based on its css name
     * + add a 1s transition animation on 
     * that style
     * @param el: the jquery element we're 
     *            working on. it must have 
     *            an html id
     * @param pr: the property's css name
     */
    const elId = $(el).attr("id")
    if ( elId !== undefined ) {
      // add the el's html id to `imgStyleSave` if needed  
      !( Object.keys(imgStyleSave).includes(elId) ) 
      ? imgStyleSave[elId] = {} : true ;
      
      // add the styles to `imgStyleSave`; allow to have
      // multiple values for the `transition` propriety
      imgStyleSave[elId][pr] = $(el).css(pr);
      Object.keys(imgStyleSave[elId]).includes("transition")
      ? imgStyleSave[elId]["transition"] += `, ${pr} 0.5s`
      : imgStyleSave[elId]["transition"] = `${pr} 0.5s`;
      
    } else {
      console.log(`element has no id: ${el}`);
    }  
  }
  
  
  function emptyStyle() {
    /**
     * empty `imgStyleSave`
     */
    imgStyleSave = {}
  }
  
  
  /**
   * hover: add a small margin around the image
   * to slightly displace the image
   */
  img.hover(
    (e) => {
      if ( clicked === false ) {
        // change the style on hover in: move the element
        // by adding 10px to `padding-left` + `padding-bottom`
        const evented = $(e.target);
        evId = $(evented).attr("id");
        saveStyle(evented, "padding-bottom");
        saveStyle(evented, "padding-left");
        evented.css({
          "padding-bottom": `${
            20 + getNoFromCss(imgStyleSave[evId]["padding-bottom"])
          }px`,
          "padding-left": `${
            20 + getNoFromCss(imgStyleSave[evId]["padding-left"])
          }px`,
        });
      }
    }
    , (e) => {
      if ( clicked === false ) {
        // change the style on hover out: revert the paddings
        // back to their original value
        const evented = $(e.target);
        evented.css( imgStyleSave[$(evented).attr("id")] );
        emptyStyle();
      }
    }
  )
  
  
  /**
   * click on an image
   */ 
  img.on("click", (e) => { 
    const evented = e.target;
    const evId = $(evented).attr("id");
    var evIdx;
    
    // get the position of the clicked image in the `img` array
    for ( let i=0; i < img.length; i++ ) {
      $(img[i]).attr("id") === evId ? evIdx = i : false;
    };
    
    if ( clicked === false ) {
      // if the image wasn't clicked before, do a focus
      
      // set the css of the clicked image
      console.log(evented);
      ["position", "margin-top", "margin-left", "rotate"
       , "width", "height"].forEach( (p) => {
        saveStyle(evented, p);
      });
      $(evented).css({ 
        position: "fixed",
        "margin-top": $(evented).offsetTop,
        "margin-left": $(evented).offsetLeft,
        "rotate": "unset",
        "height": "90vh",
        // "margin-top": $(evented)
     });
      
      for ( let i=0; i < img.length; i++ ) {
        if ( i < evIdx ) {
          // the current image is above `evented` => move it up
          saveStyle(img[i], "padding-bottom");
          $(img[i]).css({ "padding-bottom": "100vh" })
        } else if ( i > evIdx ) {
          // the current image is below `evented` => move it down
          saveStyle(img[i], "padding-top");
          $(img[i]).css({ "padding-top": "100vh" })
        }
      };
      clicked = true;
      
    } else {
      // if the image was aldready clicked, revert to a normal position
      for ( let i=0; i < img.length; i++ ) {
        $(img[i]).css( imgStyleSave[ $(img[i]).attr("id") ] )
      };
      emptyStyle();
      clicked = false;
      
    }
    
  })
  
})
