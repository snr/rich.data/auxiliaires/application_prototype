# preuve de concept: visualisation graphique de l'évolution d'une parcelle dans le temps

ceci est une preuve de concept minimale de ce à quoi pourrait ressembler la représentation
graphique de l'évolution d'une parcelle dans le temps: 
- les différentes parcelles sont superposées l'une au dessus de l'autre avec sur la droite 
  une frise chonologique (qui n'a pas été développée ici); la parcelle la plus récente est 
  en haut, la plus ancienne en bas
- en cliquant sur une parcelle, celle-ci s'affiche en grand. un cartel informatif avec des
  renvois vers d'autres éléments ressortirait alors.

pour des raisons de facilité, c'est ici la même parcelle superposée 3 fois qui a été utilisée.

développé en janvier 2023 par Paul Kervegan. le code est disponible sous la licence GNU GPL v3.

