#!/usr/bin/env bash

# this script launches the backend

root=$(pwd);
pyenv=$(cd $root/backend && ls | grep -E "^env")
cd $root/backend && source $pyenv/bin/activate && python run.py

