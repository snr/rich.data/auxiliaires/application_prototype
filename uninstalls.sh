#!/usr/bin/env bash

# uninstall angular
npm uninstall -g @angular/cli
npm cache clean --force

# uninstall nodejs
source="/etc/apt/sources.list.d/nodesource.list"
apt-get purge nodejs &&\
if [ -d "$source" ]; then
  rm -r "$source";
fi;
apt-get autoremove
