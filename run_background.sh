#!/usr/bin/env bash

cat<<EOF 
this script launches the backend and frontend in 
the background. to terminate the scripts, launch 
  " ps | grep -E '(python|ng)' "
and terminate relevant processses with
  " kill [PID] "

EOF

root=$(pwd)

cd $root/backend && source $(ls | grep -E "^env")/bin/activate  && nohup python run.py &

cd $root/frontend/ && nohup ng serve &

echo "application launched."
